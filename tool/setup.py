from setuptools import find_packages, setup

if __name__ == "__main__":
    setup(
        name="urist-tool",
        version="1.0.0",
        url="https://example.com",
        author="Author Name",
        author_email="author@gmail.com",
        description="Description of my package",
        packages=find_packages(),
        install_requires=[],
        entry_points={"console_scripts": ["ur=ur:main"],},
    )
