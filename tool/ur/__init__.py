#!/usr/bin/env python

import logging
import re
import shutil
import subprocess
import sys
from pathlib import Path
from typing import Any

import click

import urist.app
import urist.cfg
from urist.cli import main as urist_main

log = logging.getLogger("ur")
PROJECT_ROOT = Path(__file__).parent.parent.parent
SRC_ROOT = PROJECT_ROOT.joinpath("src")
TOOLSRC_ROOT = PROJECT_ROOT.joinpath("tool/ur")
BUILD_ROOT = PROJECT_ROOT.joinpath("build")
REPORT_ROOT = BUILD_ROOT.joinpath("report")


@click.group()
@click.option(
    "-v",
    "--verbose",
    "verbosity",
    count=True,
    default=int(logging.INFO / 10),
    show_default="INFO",
)
def main(verbosity: int,) -> None:
    """\b
                DEV!
            .  .                   .        ▓▓▓▓▓▓▓▓▓▓▓▓
            |  |         o         |      ▓▓            ▓▓
            |  |   ;-.   .   ,-.   |-     ▓▓            ▓▓
            |  |   |     |   `-.   |      ▓▓  ██    ██  ▓▓
            `--`   '     '   `-'   `-'    ██            ██
                                          ████████████████
                                          ████  ▓▓▓▓  ████
                Helping newbs play        ████████████████
                    Dwarf Fortress          ████████████
                        since 2019              ██████
                                                  ████
        See below for subcommands.
    """
    logging.basicConfig(**logging_configuration(verbosity))
    REPORT_ROOT.mkdir(parents=True, exist_ok=True)

    log.debug("interpreter : {!s}".format(sys.executable))
    log.debug("project root: {!s}".format(PROJECT_ROOT.resolve()))
    log.debug("source root : {!s}".format(SRC_ROOT.resolve()))


@main.command()
def check_dfhack_releases():
    from github import Github
    import urllib.request
    import urllib.error
    import urllib.parse
    import json
    from ur.hash import hash_for_file
    import datetime

    import urist.app
    import urist.cfg
    from urist.artifact.component import ComponentKind
    from urist.artifact.version import Version
    from urist.artifact.architecture import Architecture
    from urist.artifact.osfamily import OSFamily
    from urist.artifact.graphics import GraphicalImplementation

    config = urist.cfg.Env(data_dir=Path(PROJECT_ROOT, "tmp", "lnp"),)
    urist = urist.app.make_urist(config)

    working_dir = Path("tmp")
    working_dir.mkdir(exist_ok=True, parents=True)

    g = Github()
    dfhack_repo = g.get_repo("DFHack/dfhack")

    releases = []

    count_max = 15
    count = 0

    for release in dfhack_repo.get_releases():
        m = re.match(r"DFHack (?P<version>\d\.\d+.\d+[\-\.a-z0-9]+)", release.title)

        dfhack_version = m.group("version")
        df_version = dfhack_version.split("-")[0]
        release_info = {
            "kind": "dfhack",
            "version": dfhack_version,
            "df_version": df_version,
            "assets": [],
        }

        known_assets = {
            asset.url: asset
            for asset in urist.database.query_assets(
                kind=ComponentKind.DFHACK, version=Version(dfhack_version)
            ).all()
        }

        for asset in release.get_assets():
            if asset.browser_download_url not in known_assets:
                archive_name = Path(
                    urllib.parse.urlparse(asset.browser_download_url).path
                ).name
                destination_path = working_dir.joinpath(archive_name)
                if not destination_path.exists():
                    try:
                        with urllib.request.urlopen(
                            asset.browser_download_url
                        ) as response, destination_path.open("wb") as out_file:
                            data = response.read()
                            out_file.write(data)
                    except urllib.error.HTTPError:
                        continue

                if "linux" in archive_name.lower():
                    osfamily = OSFamily.LINUX
                elif "osx" in archive_name.lower():
                    osfamily = OSFamily.OSX
                else:
                    osfamily = OSFamily.WINDOWS

                if "-32-" in archive_name.lower():
                    architecture = Architecture.X86
                else:
                    architecture = Architecture.X64

                infos = {
                    "archive_name": archive_name,
                    "version": dfhack_version,
                    "download_url": asset.browser_download_url,
                    "release_url": release.url,
                    "asset_url": asset.url,
                    "size": destination_path.stat().st_size,
                    "released_date": asset.created_at.replace(
                        tzinfo=datetime.timezone.utc
                    ).isoformat(),
                    "df_version": df_version,
                    "osfamily": osfamily.value,
                    "architecture": architecture.value,
                    "graphics": GraphicalImplementation.SDL.value,
                    "kind": "dfhack",
                    "small": False,
                }

                for algo in ["md5", "sha1", "sha256", "sha384", "sha512"]:
                    with destination_path.open("rb") as opened:
                        sum = hash_for_file(opened, algo)
                        infos[f"{algo}sum"] = sum.hexdigest()

                release_info["assets"].append(infos)
            else:
                break

        if release_info["assets"]:
            releases.append(release_info)

        count += 1
        if count >= count_max:
            break

    print(json.dumps(releases, indent="  "))


@main.command()
@click.argument("version")
@click.argument("date")
def analyse_df_release(version, date) -> None:
    import urllib.request
    import urllib.error
    import urllib.parse
    import json
    from ur.hash import hash_for_file
    from urist.artifact.architecture import Architecture
    from urist.artifact.osfamily import OSFamily
    from urist.artifact.graphics import GraphicalImplementation

    url_template = "http://www.bay12games.com/dwarves/df_{minor!s}_{patch!s}_{buildtype_slug!s}.{ext!s}"

    working_dir = Path("tmp")
    working_dir.mkdir(exist_ok=True, parents=True)

    buildtype_slugs = {
        "legacy": {
            "architecture": Architecture.X64,
            "osfamily": OSFamily.WINDOWS,
            "graphics": GraphicalImplementation.LEGACY,
            "ext": "zip",
        },
        "legacy32": {
            "architecture": Architecture.X86,
            "osfamily": OSFamily.WINDOWS,
            "graphics": GraphicalImplementation.LEGACY,
            "ext": "zip",
        },
        "win": {
            "architecture": Architecture.X64,
            "osfamily": OSFamily.WINDOWS,
            "graphics": GraphicalImplementation.SDL,
            "ext": "zip",
        },
        "win32": {
            "architecture": Architecture.X86,
            "osfamily": OSFamily.WINDOWS,
            "graphics": GraphicalImplementation.SDL,
            "ext": "zip",
        },
        "linux": {
            "architecture": Architecture.X64,
            "osfamily": OSFamily.LINUX,
            "graphics": GraphicalImplementation.SDL,
            "ext": "tar.bz2",
        },
        "linux32": {
            "architecture": Architecture.X86,
            "osfamily": OSFamily.LINUX,
            "graphics": GraphicalImplementation.SDL,
            "ext": "tar.bz2",
        },
        "osx": {
            "architecture": Architecture.X64,
            "osfamily": OSFamily.OSX,
            "graphics": GraphicalImplementation.SDL,
            "ext": "tar.bz2",
        },
        "osx32": {
            "architecture": Architecture.X86,
            "osfamily": OSFamily.OSX,
            "graphics": GraphicalImplementation.SDL,
            "ext": "tar.bz2",
        },
    }

    major, minor, patch = version.split(".")

    release = {"kind": "df", "version": version, "df_version": version, "assets": []}

    for slug, build_infos in buildtype_slugs.items():
        url = url_template.format(
            minor=minor, patch=patch, buildtype_slug=slug, ext=build_infos["ext"]
        )

        archive_name = Path(urllib.parse.urlparse(url).path).name
        destination_path = working_dir.joinpath(archive_name)
        try:
            filename, _ = urllib.request.urlretrieve(url,)
            with urllib.request.urlopen(url) as response, destination_path.open(
                "wb"
            ) as out_file:
                data = response.read()
                out_file.write(data)
        except urllib.error.HTTPError:
            continue

        infos = {
            "archive_name": archive_name,
            "version": f"0.{minor}.{patch}",
            "download_url": url,
            "release_url": "http://www.bay12games.com/dwarves/older_versions.html",
            "asset_url": "http://www.bay12games.com/dwarves/older_versions.html",
            "size": destination_path.stat().st_size,
            "released_date": date,
            "df_version": f"0.{minor}.{patch}",
            "osfamily": build_infos["osfamily"].value,
            "architecture": build_infos["architecture"].value,
            "graphics": build_infos["graphics"].value,
            "kind": "df",
            "small": False,
        }

        for algo in ["md5", "sha1", "sha256", "sha384", "sha512"]:
            with destination_path.open("rb") as opened:
                sum = hash_for_file(opened, algo)
                infos[f"{algo}sum"] = sum.hexdigest()

        release["assets"].append(infos)

    print(json.dumps(release, indent="  "))


@main.command()
def labz():
    """
    Sandbox!
    """
    import json
    from urist.artifact.version import Version
    from collections import OrderedDict

    with SRC_ROOT.joinpath("urist/db/db.json").open() as orig:
        db = json.load(orig)

    new_db = {
        "components": {},
        "releases": {},
        "assets": {}
    }

    components = db["components"]

    for slug, component in components.items():
        assert slug not in new_db["components"]
        new_db["components"][slug] = component
        new_db["components"][slug]["slug"] = slug

    for release in db["releases"]:
        release_component = release["kind"]
        release_version = release["version"]
        release_slug = f"{release_component}-{release_version}"
        assert release_slug not in new_db["releases"]
        new_db["releases"][release_slug] = {
            "slug": release_slug,
            "component": release_component,
            "version": release_version,
            "df_version": release["df_version"],
        }

    for release in db["releases"]:
        release_component = release["kind"]
        release_version = release["version"]
        release_slug = f"{release_component}-{release_version}"
        for asset in release["assets"]:
            asset_osfamily = asset["osfamily"]
            asset_architecture = asset["architecture"]
            asset_graphics = asset["graphics"]

            archive_name = asset["archive_name"]

            match = re.match(r".+(?P<compiler>(gcc-(\d(\.\d)*))).+", archive_name)
            if match:
                compiler = match.group("compiler")
                asset_slug = f"{release_slug}-{asset_osfamily}-{asset_architecture}-{asset_graphics}-{compiler}"
            else:
                compiler = None
                asset_slug = f"{release_slug}-{asset_osfamily}-{asset_architecture}-{asset_graphics}"

            if asset["small"]:
                asset_slug = f"{asset_slug}-small"

            assert asset_slug not in new_db["assets"], f"Asset {asset_slug} already exists for archive {archive_name}"
            new_db["assets"][asset_slug] = {
                "slug": asset_slug,
                "release": release_slug,
                "download_url": asset["download_url"],
                "release_url": asset["release_url"],
                "asset_url": asset["asset_url"],
                "size": asset["size"],
                "released_date": asset["released_date"],
                "osfamily": asset["osfamily"],
                "architecture": asset["architecture"],
                "graphics": asset["graphics"],
                "small": asset["small"],
                "compiler": compiler
            }

            try:
                sums = {
                    "md5sum": asset["md5sum"],
                    "sha1sum": asset["sha1sum"],
                    "sha256sum": asset["sha256sum"],
                    "sha384sum": asset["sha384sum"],
                    "sha512sum": asset["sha512sum"],
                    "small": asset["sha512sum"],
                }
                new_db["assets"][asset_slug].update(sums)
            except KeyError:
                log.warning(f"No sums for {asset_slug}.")

    from urist.io import yaml

    with Path("newdb.yaml").open("w") as opened:
        yaml.dump(new_db, opened)


@main.command()
def rtfd() -> None:
    """
    Open documentation in browser.
    """
    click.launch(str(BUILD_ROOT.joinpath("html/index.html")))


@main.command()
def clean():
    """
    Clean some files.
    """

    def clean_bytecode(here: Path):
        log.info("cleaning bytecode at {!s}".format(here))
        for path in here.rglob("*.py[co]"):
            path.unlink()
            log.info("  cleaned {!s}".format(path))

    def clean_pycache(here: Path):
        log.info("cleaning pycache at {!s}".format(here))
        for path in here.rglob("__pycache__"):
            path.rmdir()
            log.info("  cleaned {!s}".format(path))

    def clean_other_cache(here: Path):
        log.info("cleaning other cache at {!s}".format(here))
        for path in here.glob("*_cache"):
            shutil.rmtree(str(path))
            log.info("  cleaned {!s}".format(path))

    where = PROJECT_ROOT
    log.info("cleaning {!s}".format(where))
    clean_bytecode(where)
    clean_pycache(where)
    clean_other_cache(where)


@main.command()
def write_requirements():
    """
    Write requirements.txt file.
    """
    import configparser

    project_info = configparser.ConfigParser()
    project_info.read(str(PROJECT_ROOT / "setup.cfg"))

    outputfile = PROJECT_ROOT / "requirements.txt"

    deps = [
        dep.strip() + os.linesep
        for dep in project_info["options"]["install_requires"].splitlines()
        if dep
    ]

    log.debug("writing requirements to '{!s}'".format(outputfile))

    with Path(outputfile).open("w") as opened:
        opened.writelines(deps)


@main.group("format", invoke_without_command=True)
@click.pass_context
def reformat(ctx: click.Context) -> None:
    """
    Format code.

    When no commands are provided, all commands are run.
    """
    if ctx.invoked_subcommand is None:
        run_subcommands(imports, code)


@reformat.command()
def imports() -> None:
    """
    Reorders python imports.
    """
    paths = [
        PROJECT_ROOT.joinpath("tool"),
        PROJECT_ROOT.joinpath("src"),
        PROJECT_ROOT.joinpath("test"),
    ]

    cmd = ["isort", "-rc"]
    cmd.extend(paths)  # type: ignore

    log.info("Running 'isort' to reorder python imports")
    result = run_cmd(cmd)
    raise click.exceptions.Exit(result.returncode)


@reformat.command()
def code() -> None:
    """
    Reformat python code.
    """
    paths = [
        PROJECT_ROOT.joinpath("tool"),
        PROJECT_ROOT.joinpath("src"),
        PROJECT_ROOT.joinpath("test"),
    ]

    cmd = ["black"]
    cmd.extend(paths)  # type: ignore

    log.info("Running 'black' to reformat python code")
    result = run_cmd(cmd)
    raise click.exceptions.Exit(result.returncode)


@main.group(invoke_without_command=True)
@click.pass_context
def check(ctx: click.Context) -> None:
    """
    Perform checks.

    When no commands are provided, all commands are run.
    """
    if ctx.invoked_subcommand is None:
        run_subcommands(test, style, typing, security, lint, complexity, report)


@check.command()
def test() -> None:
    """
    Run the tests.
    """
    log.info("Running 'pytest' to run tests")
    run_cmd(["pytest"])


@check.command()
def style():
    """
    Check code style.
    """
    paths = [SRC_ROOT, TOOLSRC_ROOT, PROJECT_ROOT.joinpath("test")]

    cmd = ["black", "-l80", "--check", "--diff"]
    cmd.extend(paths)
    run_cmd(cmd)


@check.command()
def typing() -> None:
    """
    Check type annotations.
    """
    paths = [
        TOOLSRC_ROOT,
        SRC_ROOT,
    ]
    cmd = ["mypy"]
    cmd.extend(paths)  # type: ignore
    run_cmd(cmd)


@check.command()
@click.pass_context
def report(ctx: click.Context) -> None:
    """
    Aggregate test coverage reports.
    """
    run_cmd(["coverage", "report"])
    run_cmd(["coverage", "html"])
    run_cmd(["coverage", "xml"])
    combine_junit_reports()


@check.command()
def security() -> None:
    """
    Check for security vulnerabilities.
    """
    paths = [SRC_ROOT]

    cmd = ["bandit", "-r"]
    cmd.extend(paths)
    run_cmd(cmd)

    cmd = ["bandit", "-f", "json", "-o", REPORT_ROOT.joinpath("sast.json")]
    cmd.extend(paths)
    run_cmd(cmd)


@check.command()
def complexity() -> None:
    """
    Check code complexity with Xenon.
    """
    paths = [
        PROJECT_ROOT.joinpath("src"),
    ]
    cmd = [
        "xenon",
        "--max-average",
        "A",
        "--max-modules",
        "A",
        "--max-absolute",
        "A",
    ]
    cmd.extend(paths)  # type: ignore

    log.info("Running 'xenon' to check for complexity")
    run_cmd(cmd)


@check.command()
def lint():
    """
    Check for smells in code.
    """
    log.info("Running 'xenon' to check for complexity")
    paths = [
        SRC_ROOT,
        PROJECT_ROOT.joinpath("test"),
    ]

    cmd = [
        "flake8",
        "--exit-zero",
        "--format junit-xml",
        "--output-file build/reports/codeclimate.json",
    ]

    format = [
        "--format",
        "junit-xml",
        "--output-file",
        "build/reports/codeclimate.json",
    ]
    cmd.extend(format)
    cmd.extend(paths)
    run_cmd(cmd)

    format = [
        "--format",
        "junit-xml",
        "--output-file",
        "build/reports/flake8_junit.json",
    ]
    cmd.extend(format)
    cmd.extend(paths)
    run_cmd(cmd)


@main.group(invoke_without_command=True)
@click.pass_context
def build(ctx: click.Context) -> None:
    """
    Assemble artifacts.
    """
    if ctx.invoked_subcommand is None:
        run_subcommands(doc, wheel, sdist, standalone)


@build.command()
def wheel() -> None:
    """
    Build binary wheel distribution.
    """
    cmd = [sys.executable, PROJECT_ROOT.joinpath("setup.py"), "bdist_wheel"]
    run_cmd(cmd)


@build.command()
def sdist() -> None:
    """
    Build source distribution.
    """
    cmd = [sys.executable, PROJECT_ROOT.joinpath("setup.py"), "sdist"]
    run_cmd(cmd)


@build.command()
def standalone() -> None:
    """
    Build standalone executable.

    FIXME: only Linux is implemented (and not really tested anyways...)
    """
    try:
        import PyInstaller.__main__
    except ImportError:
        log.critical("You need to install pyinstaller.")
        raise click.exceptions.Exit(1)

    pyinstaller = PyInstaller.__main__
    name = "{!s}-{!s}-{!s}".format(
        urist.__dist__.project_name, urist.__dist__.version, "linux"
    )
    pyinstaller.run(
        [
            "--name",
            "{!s}".format(name),
            "--onefile",
            "--clean",
            "--paths",
            str(SRC_ROOT),
            str(SRC_ROOT.joinpath(urist.__dist__.project_name, "__main__.py")),
        ]
    )


@build.command()
def doc() -> None:
    """
    Build PDF and HTML documentation.
    """
    DOC_ROOT = PROJECT_ROOT.joinpath("doc")
    BUILD_ROOT = PROJECT_ROOT.joinpath("build", "sphinx")
    artifact_stem = "{!s}-{!s}".format(
        urist.__dist__.project_name, urist.__dist__.version
    )

    def sphinx(builder: str):
        get_sphinx()(
            [
                "-b",
                builder,
                "-d",
                str(BUILD_ROOT.joinpath("doctrees")),
                "-n",
                "-T",
                str(DOC_ROOT),
                str(BUILD_ROOT.joinpath(builder)),
            ]
        )

    def get_sphinx():
        try:
            import sphinx.cmd.build
        except ImportError:
            log.critical("You need to install sphinx.")
            sys.exit(1)

        return sphinx.cmd.build.main

    def get_apidoc():
        try:
            import sphinx.ext.apidoc
        except ImportError:
            log.critical("You need to install sphinx.")
            sys.exit(1)
        return sphinx.ext.apidoc.main

    DOC_ROOT.joinpath("_static").mkdir(parents=True, exist_ok=True)
    apidoc = get_apidoc()
    apidoc(
        [
            "--module-first",
            "--separate",
            "--maxdepth",
            "4",
            "--output-dir",
            str(DOC_ROOT),
            "--force",
            str(SRC_ROOT),
        ]
    )
    sphinx("latex")
    subprocess.run(["make"], cwd=str(BUILD_ROOT.joinpath("latex")))
    shutil.copy2(
        str(BUILD_ROOT.joinpath("latex", "{!s}.pdf".format(artifact_stem))),
        str(DOC_ROOT.joinpath("_static")),
    )
    sphinx("html")
    sphinx("linkcheck")


@main.group()
def gui() -> None:
    """
    Helpers for developing the GUI.
    """
    pass


@gui.command()
def controls() -> None:
    """
    List controls in the mainwindow.
    """
    import sys
    from qtpy import QtCore, QtWidgets, QtGui
    from qtpy import uic

    import urist.app
    import urist.cfg

    config = urist.cfg.Env(data_dir=Path(PROJECT_ROOT, "tmp", "lnp"),)
    urist = urist.app.make_urist(config)
    _ = QtWidgets.QApplication(sys.argv)

    class UristUI(QtCore.QObject):
        def __init__(self, ui_file, parent=None):
            super().__init__(parent=parent)

            ui_file = QtCore.QFile(str(ui_file))
            ui_file.open(QtCore.QFile.ReadOnly)

            self.window = uic.loadUi(ui_file)

    ui = UristUI(str(SRC_ROOT.joinpath("urist/gui/ui/urist.ui")))
    for control in ui.window.findChildren(QtWidgets.QWidget):
        name = control.objectName()
        if (
            name
            and not name.startswith("qt")
            and name not in urist.settings_definitions
        ):
            print(name)


@main.group()
def migration() -> None:
    """
    Manage the database migrations.
    """
    pass


@migration.command()
def current() -> None:
    """
    Display current database migration revision.
    """
    import urist.app
    import urist.cfg

    config = urist.cfg.Env(data_dir=Path(PROJECT_ROOT, "tmp", "lnp"),)
    urist = urist.app.make_urist(config)

    print(urist.database.revision)


@migration.command()
@click.option(
    "-r",
    "--revision",
    "revision",
    default="head",
    help="Revision ID up to which to upgrade.",
    show_default=True,
)
def upgrade(revision: str) -> None:
    """
    Apply database migrations.
    """
    import urist.app
    import urist.cfg

    config = urist.cfg.Env(data_dir=Path(PROJECT_ROOT, "tmp", "lnp"),)
    urist = urist.app.make_urist(config)

    urist.database.migrate(revision)


@migration.command()
def generate() -> None:
    """
    Generate a new database migration script.
    """
    import alembic.util
    import alembic.config
    import alembic.command
    import sqlalchemy.exc

    import urist.app
    import urist.cfg

    config = urist.cfg.Env(data_dir=Path(PROJECT_ROOT, "tmp", "lnp"),)
    urist = urist.app.make_urist(config)

    script_location = SRC_ROOT.parent.joinpath("urist/db/migration")

    alembic_config = alembic.config.Config()
    alembic_config.set_main_option("script_location", str(script_location))

    log.debug("using database at '{!s}'".format(urist.database.engine.url))
    log.debug("generating migration for '{!s}'".format(script_location))

    try:
        with urist.database.engine.begin() as connection:
            alembic_config.attributes["connection"] = connection
            rev = alembic.command.revision(
                alembic_config, "initial", autogenerate=True,
            )
            print(rev)
    except sqlalchemy.exc.OperationalError as error:
        if "unable to open database file" in error.orig.args:
            log.error("The database file could not be opened.")
            log.error("This can be because the URI provided is wrong.")
            raise click.Abort() from error
        raise
    except alembic.util.exc.CommandError as error:
        if "Target database is not up to date." in error.args:
            log.error("Target database is not up to date.")
            log.error(
                "You need to update it to the latest revision "
                "before generating a new revision."
            )
            log.error(
                "You can do so by launching the appliation which "
                "should update the database on startup"
            )
            raise click.Abort() from error
    except Exception as error:
        log.error("Something went terribly wrong :,-(")
        log.error(error.args)
        raise


def combine_junit_reports():
    from junitparser import JUnitXml

    where = PROJECT_ROOT / "build" / "reports"

    final_report = JUnitXml("urist")

    for report in where.glob("*junit.xml"):
        log.info(f"appending {report!s}")
        final_report += JUnitXml.fromfile(str(report))

    final_report.write(where.joinpath("junit.xml"), pretty=True)


def run_cmd(cmd, cwd=PROJECT_ROOT):
    result = subprocess.run([str(part) for part in cmd], cwd=cwd)
    if result.returncode:
        raise click.Abort()
    return result


def run_subcommands(*subcommands: click.Command):
    ctx = click.get_current_context()
    failed = []
    for cmd in subcommands:
        try:
            ctx.invoke(cmd)
        except click.exceptions.Exit as error:
            if error.exit_code:
                failed.append(cmd)

    exit_code = 0

    if failed:
        click.echo("")
        click.echo("---------------", err=True)
        click.echo("Command failed:", err=True)
        for fail in failed:
            click.echo(f"  {fail.name}", err=True)

        exit_code = 1

    raise click.exceptions.Exit(exit_code)


def logging_configuration(verbosity: int) -> Any:
    level = logging.CRITICAL - logging.DEBUG * verbosity

    if level < logging.DEBUG:
        format = "%(levelname)8s %(name)-24s> %(message)s"
    else:
        format = "%(levelname)8s: %(message)s"

    return dict(level=level, format=format,)


main.add_command(urist_main, name="ist")


if __name__ == "__main__":
    main(auto_envvar_prefix="DEVURIST", prog_name="ur")
