import datetime as dt
import hashlib
import io
import logging
import math
import sys
from pathlib import Path

MULTIPLES = ["B", "k{}B", "M{}B", "G{}B", "T{}B", "P{}B", "E{}B", "Z{}B", "Y{}B"]


def hash_for_file(
    fileobj, algorithm: str = "sha1", block_size: int = io.DEFAULT_BUFFER_SIZE
):
    """
    Compute the checksum of a file object.

    Args:
        fileobj: File-like object.
        algorithm: Hashing algorithm to use.
        block_size: Read the file by chunks of this size.
    """
    if algorithm not in hashlib.algorithms_available:
        raise NameError('"{algorithm}" is not available.'.format(algorithm=algorithm))

    # According to hashlib documentation using new()
    # will be slower than calling using named
    # constructors, ex.: hashlib.md5()
    hasher = hashlib.new(algorithm)

    for chunk in iter(lambda: fileobj.read(block_size), b""):
        hasher.update(chunk)

    return hasher


def humanbytes(i, binary=False, precision=2):
    base = 1024 if binary else 1000
    multiple = math.trunc(math.log2(i) / math.log2(base))
    value = i / math.pow(base, multiple)
    suffix = MULTIPLES[multiple].format("i" if binary else "")
    return f"{value:.{precision}f} {suffix}"
