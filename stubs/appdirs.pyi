from typing import Any, Optional

__version_info__: Any
PY3: Any
unicode = str
system: Any

def user_data_dir(
    appname: Optional[Any] = ...,
    appauthor: Optional[Any] = ...,
    version: Optional[Any] = ...,
    roaming: bool = ...,
) -> str: ...
def site_data_dir(
    appname: Optional[Any] = ...,
    appauthor: Optional[Any] = ...,
    version: Optional[Any] = ...,
    multipath: bool = ...,
) -> str: ...
def user_config_dir(
    appname: Optional[Any] = ...,
    appauthor: Optional[Any] = ...,
    version: Optional[Any] = ...,
    roaming: bool = ...,
) -> str: ...
def site_config_dir(
    appname: Optional[Any] = ...,
    appauthor: Optional[Any] = ...,
    version: Optional[Any] = ...,
    multipath: bool = ...,
) -> str: ...
def user_cache_dir(
    appname: Optional[Any] = ...,
    appauthor: Optional[Any] = ...,
    version: Optional[Any] = ...,
    opinion: bool = ...,
) -> str: ...
def user_state_dir(
    appname: Optional[Any] = ...,
    appauthor: Optional[Any] = ...,
    version: Optional[Any] = ...,
    roaming: bool = ...,
) -> str: ...
def user_log_dir(
    appname: Optional[Any] = ...,
    appauthor: Optional[Any] = ...,
    version: Optional[Any] = ...,
    opinion: bool = ...,
) -> str: ...

class AppDirs:
    appname: Any = ...
    appauthor: Any = ...
    version: Any = ...
    roaming: Any = ...
    multipath: Any = ...
    def __init__(
        self,
        appname: Optional[Any] = ...,
        appauthor: Optional[Any] = ...,
        version: Optional[Any] = ...,
        roaming: bool = ...,
        multipath: bool = ...,
    ) -> None: ...
    @property
    def user_data_dir(self) -> str: ...
    @property
    def site_data_dir(self) -> str: ...
    @property
    def user_config_dir(self) -> str: ...
    @property
    def site_config_dir(self) -> str: ...
    @property
    def user_cache_dir(self) -> str: ...
    @property
    def user_state_dir(self) -> str: ...
    @property
    def user_log_dir(self) -> str: ...
