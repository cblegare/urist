:orphan:

.. _glossary:

Glossary
========

.. glossary::
    :sorted:

    path-like

        See https://docs.python.org/3/glossary.html#term-path-like-object.
