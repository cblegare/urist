#######
|urist|
#######

.. only:: html

    |home_badge| |doc_badge| |lic_badge| |pypi_badge| |downloads_badge|

    .. |lic_badge| image:: https://img.shields.io/badge/License-LGPL%20v3-blue.svg
        :target: http://www.gnu.org/licenses/lgpl-3.0
        :alt: GNU Lesser General Public License v3, see :ref:`license`.

    .. |home_badge| image:: https://img.shields.io/badge/gitlab-cblegare%2Furist-orange.svg
        :target: https://gitlab.com/cblegare/urist
        :alt: Home Page

    .. |pypi_badge| image:: https://badge.fury.io/py/urist.svg
        :target: https://pypi.org/project/urist/
        :alt: PyPI Package

    .. |downloads_badge| image:: https://img.shields.io/pypi/dm/urist.svg
        :target: https://pypi.org/project/urist/
        :alt: PyPI Downloads

    .. |doc_badge| image:: https://img.shields.io/badge/docs-urist-blue.svg
        :target: https://cblegare.gitlab.io/urist
        :alt: Home Page

**yur·rist** /ˈjurɪst/

    "Urist" is the dwarven word for "dagger".
    
    Urist is also the tongue-in-cheek term for the temperature units 
    used by Toady One and has since been adopted by players. 
    
    It has also expanded to be a unit of measurement for any dwarven 
    measuring system.

    *"the room was 100 square Urists"*

Browse also the `project's home`_.


.. only:: latex

    The latest version of this document is also available online in the Docs_.

    .. _Docs: https://cblegare.gitlab.io/urist/


.. only:: html

    The latest version of this document is also available as a |pdflink|.


.. toctree::
    :maxdepth: 2

    usage
    about
    changelog
    authors
    license


======================
Technical Informations
======================

.. only:: html

    |build_badge| |cov_badge|

    .. |build_badge| image:: https://gitlab.com/cblegare/urist/badges/master/pipeline.svg
        :target: https://gitlab.com/cblegare/urist/pipelines
        :alt: Build Status

    .. |cov_badge| image:: https://gitlab.com/cblegare/urist/badges/master/coverage.svg
        :target: _static/embedded/coverage/index.html
        :alt: Coverage Report

Here lies guides and references for contributors as well as some general
references.

.. toctree::
    :maxdepth: 2

    contributing
    arch/index
    modules
    glossary
    release


.. only:: html

    ==================
    Tables and indices
    ==================

    Quick access for not so much stuff.

    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`



.. _project's home: https://gitlab.com/cblegare/urist/
