#!/usr/bin/env python

import logging
import os
import shutil
import subprocess
import sys
from pathlib import Path
from typing import Any

import click

import urist.app
import urist.cfg

log = logging.getLogger("devurist")
PROJECT_ROOT = Path(__file__).parent
SRC_ROOT = PROJECT_ROOT.joinpath("src")
BUILD_ROOT = PROJECT_ROOT.joinpath("build")
REPORT_ROOT = BUILD_ROOT.joinpath("report")


@click.group(name="uristctl")
@click.version_option(version=urist.__dist__.version)
@click.option("-v", "--verbose", "verbosity", count=True)
@click.pass_context
def main(ctx: click.Context, verbosity: int,) -> None:
    """\b
                DEV!
            .  .                   .        ▓▓▓▓▓▓▓▓▓▓▓▓
            |  |         o         |      ▓▓            ▓▓
            |  |   ;-.   .   ,-.   |-     ▓▓            ▓▓
            |  |   |     |   `-.   |      ▓▓  ██    ██  ▓▓
            `--`   '     '   `-'   `-'    ██            ██
                                          ████████████████
                                          ████  ▓▓▓▓  ████
                Helping newbs play        ████████████████
                    Dwarf Fortress          ████████████
                        since 2019              ██████
                                                  ████
        See below for subcommands.
    """
    logging.basicConfig(**logging_configuration(verbosity))
    config = urist.cfg.Env(config_dir=Path(PROJECT_ROOT, "tmp", "lnp"), )
    ctx.obj = urist.app.make_urist(config)

    REPORT_ROOT.mkdir(parents=True, exist_ok=True)

    log.debug("interpreter : {!s}".format(sys.executable))
    log.debug("project root: {!s}".format(PROJECT_ROOT.resolve()))
    log.debug("source root : {!s}".format(SRC_ROOT.resolve()))


@main.command()
def view_gui():
    import sys
    from qtpy import QtCore, QtWidgets
    from qtpy import uic
    from pathlib import Path

    app = QtWidgets.QApplication(sys.argv)

    class UristUI(QtCore.QObject):
        def __init__(self, ui_file, parent=None):
            super().__init__(parent=parent)

            ui_file = QtCore.QFile(str(ui_file))
            ui_file.open(QtCore.QFile.ReadOnly)

            self.window = uic.loadUi(ui_file)

    ui = UristUI(str(SRC_ROOT.joinpath("urist/gui/urist.ui")))
    ui.window.show()
    sys.exit(app.exec_())


@main.group()
def build():
    pass


@build.command()
def standalone():
    try:
        import PyInstaller.__main__
    except ImportError:
        log.critical("You need to install pyinstaller.")
        sys.exit(1)

    pyinstaller = PyInstaller.__main__
    name = "{!s}-{!s}-{!s}".format(
        urist.__dist__.project_name, urist.__dist__.version, "linux"
    )
    pyinstaller(
        [
            "--name",
            "{!s}".format(name),
            "--onefile",
            "--clean",
            "--paths",
            str(SRC_ROOT),
            str(SRC_ROOT.joinpath(urist.__dist__.project_name, "__main__.py")),
        ]
    )


@main.command()
def write_requirements():
    import configparser

    project_info = configparser.ConfigParser()
    project_info.read(str(PROJECT_ROOT / "setup.cfg"))

    outputfile = PROJECT_ROOT / "requirements.txt"

    deps = [
        dep.strip() + os.linesep
        for dep in project_info["options"]["install_requires"].splitlines()
        if dep
    ]

    log.debug("writing requirements to '{!s}'".format(outputfile))

    with Path(outputfile).open("w") as opened:
        opened.writelines(deps)


@main.command()
@click.pass_obj
def gen_migration(obj: urist.app.Urist):
    import alembic.util
    import alembic.config
    import alembic.command
    import sqlalchemy.exc

    script_location = SRC_ROOT.joinpath("urist/db/migration")

    alembic_config = alembic.config.Config()
    alembic_config.set_main_option("script_location", str(script_location))

    log.debug("using database at '{!s}'".format(obj.database.engine.url))
    log.debug("generating migration for '{!s}'".format(script_location))

    try:
        with obj.database.engine.begin() as connection:
            alembic_config.attributes["connection"] = connection
            rev = alembic.command.revision(
                alembic_config, "initial", autogenerate=True,
            )
            print(rev)
    except sqlalchemy.exc.OperationalError as error:
        if "unable to open database file" in error.orig.args:
            log.error("The database file could not be opened.")
            log.error("This can be because the URI provided is wrong.")
            raise click.Abort() from error
        raise
    except alembic.util.exc.CommandError as error:
        if "Target database is not up to date." in error.args:
            log.error("Target database is not up to date.")
            log.error(
                "You need to update it to the latest revision "
                "before generating a new revision."
            )
            log.error(
                "You can do so by launching the appliation which "
                "should update the database on startup"
            )
            raise click.Abort() from error
    except Exception as error:
        log.error("Something went terribly wrong :,-(")
        log.error(error.args)
        raise


@main.command()
def clean():
    def clean_bytecode(here: Path):
        log.info("cleaning bytecode at {!s}".format(here))
        for path in here.rglob("*.py[co]"):
            path.unlink()
            log.info("  cleaned {!s}".format(path))

    def clean_pycache(here: Path):
        log.info("cleaning pycache at {!s}".format(here))
        for path in here.rglob("__pycache__"):
            path.rmdir()
            log.info("  cleaned {!s}".format(path))

    def clean_other_cache(here: Path):
        log.info("cleaning other cache at {!s}".format(here))
        for path in here.glob("*_cache"):
            shutil.rmtree(str(path))
            log.info("  cleaned {!s}".format(path))

    where = PROJECT_ROOT
    log.info("cleaning {!s}".format(where))
    clean_bytecode(where)
    clean_pycache(where)
    clean_other_cache(where)


@main.command()
def doc():
    DOC_ROOT = PROJECT_ROOT.joinpath("doc")
    BUILD_ROOT = PROJECT_ROOT.joinpath("build", "sphinx")
    artifact_stem = "{!s}-{!s}".format(
        urist.__dist__.project_name, urist.__dist__.version
    )

    def sphinx(builder: str):
        get_sphinx()(
            [
                "-b",
                builder,
                "-d",
                str(BUILD_ROOT.joinpath("doctrees")),
                "-n",
                "-T",
                str(DOC_ROOT),
                str(BUILD_ROOT.joinpath(builder)),
            ]
        )

    def get_sphinx():
        try:
            import sphinx.cmd.build
        except ImportError:
            log.critical("You need to install sphinx.")
            sys.exit(1)

        return sphinx.cmd.build.main

    def get_apidoc():
        try:
            import sphinx.ext.apidoc
        except ImportError:
            log.critical("You need to install sphinx.")
            sys.exit(1)
        return sphinx.ext.apidoc.main

    DOC_ROOT.joinpath("_static").mkdir(parents=True, exist_ok=True)
    apidoc = get_apidoc()
    apidoc(
        [
            "--module-first",
            "--separate",
            "--maxdepth",
            "4",
            "--output-dir",
            str(DOC_ROOT),
            "--force",
            str(SRC_ROOT),
        ]
    )
    sphinx("latex")
    subprocess.run(["make"], cwd=str(BUILD_ROOT.joinpath("latex")))
    shutil.copy2(
        str(BUILD_ROOT.joinpath("latex", "{!s}.pdf".format(artifact_stem))),
        str(DOC_ROOT.joinpath("_static")),
    )
    sphinx("html")
    sphinx("linkcheck")


@main.command()
def combine_junit_reports():
    from junitparser import JUnitXml

    where = PROJECT_ROOT / "build" / "reports"

    final_report = JUnitXml("pylnp")

    for report in where.glob("*junit.xml"):
        log.info(f"appending {report!s}")
        final_report += JUnitXml.fromfile(str(report))

    final_report.write(where.joinpath("junit.xml"), pretty=True)


@main.group()
def format():
    pass


@format.command("all")
@click.pass_context
def all_format(ctx: click.Context):
    ctx.invoke(imports)
    ctx.invoke(code)


@format.command()
def imports():
    paths = [SRC_ROOT, __file__, PROJECT_ROOT.joinpath("test")]

    cmd = ["isort", "-rc"]
    cmd.extend(paths)
    run_cmd(cmd)


@format.command()
def code():
    paths = [SRC_ROOT, __file__, PROJECT_ROOT.joinpath("test")]

    cmd = ["black", "-l80"]
    cmd.extend(paths)
    run_cmd(cmd)


@main.group()
def check():
    pass


@check.command("all")
@click.pass_context
def all_checs(ctx: click.Context):
    ctx.invoke(test)
    ctx.invoke(style)
    ctx.invoke(typing)
    ctx.invoke(security)
    ctx.invoke(lint)
    ctx.invoke(report)


@check.command()
@click.pass_context
def report(ctx: click.Context):
    run_cmd(["coverage", "report"])
    run_cmd(["coverage", "html"])
    run_cmd(["coverage", "xml"])
    ctx.invoke(combine_junit_reports)


@check.command()
def test():
    cmd = ["pytest"]
    run_cmd(cmd)


@check.command()
def style():
    paths = [SRC_ROOT, __file__, PROJECT_ROOT.joinpath("test")]

    cmd = ["black", "-l80", "--check", "--diff"]
    cmd.extend(paths)
    run_cmd(cmd)


@check.command()
def typing():
    cmd = ["mypy", SRC_ROOT]
    run_cmd(cmd)


@check.command()
def security():
    paths = [SRC_ROOT]

    cmd = ["bandit", "-f", "json", "-o", REPORT_ROOT.joinpath("sast.json")]
    cmd.extend(paths)
    run_cmd(cmd)

    cmd = ["bandit", "-r"]
    cmd.extend(paths)
    run_cmd(cmd)


@check.command()
def lint():
    subprocess.run(
        [
            "flake8",
            "--exit-zero",
            "--format junit-xml",
            "--output-file build/reports/codeclimate.json",
            "src test",
        ],
        cwd=str(PROJECT_ROOT),
    )
    subprocess.run(
        [
            "flake8",
            "--exit-zero",
            "--format junit-xml",
            "--output-file build/reports/flake8_junit.json",
            "src test",
        ],
        cwd=str(PROJECT_ROOT),
    )


def run_cmd(cmd, cwd=PROJECT_ROOT):
    result = subprocess.run([str(part) for part in cmd], cwd=cwd)
    if result.returncode:
        raise click.Abort()
    return result


def logging_configuration(verbosity: int) -> Any:
    level = logging.CRITICAL - logging.DEBUG * verbosity

    if level < logging.DEBUG:
        format = "%(name)s: %(levelname)s %(message)s"
    else:
        format = "%(levelname)8s: %(message)s"

    return dict(
        level=level,
        format=format,
        handlers=[logging.FileHandler("stderr.log"), logging.StreamHandler()],
    )


if __name__ == "__main__":
    main(auto_envvar_prefix="DEVURIST", prog_name="devurist")
