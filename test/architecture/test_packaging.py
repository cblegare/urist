import configparser

import pkg_resources
import pytest

DIST_NAME = "urist"


def test_all_extra_contains_all_extras(setup_config, current_distribution):
    """
    Ensure that an `all` extra exists that contains all the dependencies
    from other extras.

    Note:

        This test along with the `all` extra should be removed if something
        like support for globbing syntax when specifying extras with `pip`.

        Related `pip` issue: https://github.com/pypa/pip/issues/5039.
    """
    extras = setup_config["options.extras_require"]

    all_extras = set()
    expected = set()

    for extra_name, dependencies in extras.items():
        if extra_name == "all":
            all_extras.update(dependencies.strip().splitlines())
        else:
            expected.update(dependencies.strip().splitlines())

    assert all_extras == expected


def test_extras_installed(setup_config, current_distribution):
    """
    This test ensures that configured extras dependencies are properly
    managed by :mod:`setuptools`.
    """
    extras = setup_config["options.extras_require"]
    assert set(extras.keys()) == set(current_distribution.extras)


@pytest.fixture()
def current_distribution():
    return pkg_resources.get_distribution(DIST_NAME)


@pytest.fixture()
def setup_config(project_root_dir):
    config = configparser.ConfigParser()
    config.read(str(project_root_dir / "setup.cfg"))
    return config
