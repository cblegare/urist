from urist.compat import HashableMixin


def test_equality_objects_with_one_same_value():
    class Object(HashableMixin):
        def __init__(self, value):
            self.value = value

    same_value = "some value"

    assert Object(same_value) == Object(same_value)


def test_equality_objects_with_one_different_value():
    class Object(HashableMixin):
        def __init__(self, value):
            self.value = value

    assert Object("some value") != Object("not the same_value")


def test_equality_objects_with_same_keys_different_values():
    class Object(HashableMixin):
        def __init__(self, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

    assert Object(first_key="value", second_key="other_value") != Object(
        first_key="not the same value", second_key="other value but different"
    )


def test_equality_objects_with_different_keys_same_values():
    class Object(HashableMixin):
        def __init__(self, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

    first_value = "value"
    second_value = "other_value"

    assert Object(first_key=first_value, second_key=second_value) != Object(
        first_key=first_value, second_different_key=second_value
    )


def test_equality_is_based_on_hash():
    class Object(HashableMixin):
        def __init__(self, **kwargs):
            for name, value in kwargs.items():
                setattr(self, name, value)

        def __hash__(self):
            return 1

    one_object = Object(first_key="value", second_key="other_value")
    another_really_different_object = Object(
        first_key="not the same value", second_key="other value but different"
    )

    assert one_object == another_really_different_object
