from unittest.mock import Mock

from urist.artifact.architecture import Architecture
from urist.artifact.buildtype import BuildType
from urist.artifact.graphics import GraphicalImplementation
from urist.artifact.osfamily import OSFamily


def test_match():
    sut_arch = Mock()
    sut_os = Mock()
    sut_graph = Mock()
    other_arch = Mock()
    other_os = Mock()
    other_graph = Mock()

    sut = BuildType(sut_arch, sut_os, sut_graph)
    other = BuildType(other_arch, other_os, other_graph)

    sut.match(other)

    sut_arch.match.assert_called_once_with(other_arch)
    sut_os.match.assert_called_once_with(other_os)
    sut_graph.match.assert_called_once_with(other_graph)


def test_match_agains_other_type():
    sut_arch = Mock()
    sut_os = Mock()
    sut_graph = Mock()
    sut = BuildType(sut_arch, sut_os, sut_graph)

    assert not sut.match(object)


def test_from_platform(monkeypatch):
    SpyArchitecture = Mock()
    SpyOSFamily = Mock()
    SpyGraphicalImplementation = Mock()

    monkeypatch.setattr(Architecture, "from_platform", SpyArchitecture)
    monkeypatch.setattr(OSFamily, "from_platform", SpyOSFamily)
    monkeypatch.setattr(
        GraphicalImplementation, "from_platform", SpyGraphicalImplementation
    )

    _ = BuildType.from_platform()

    SpyArchitecture.assert_called_once_with()
    SpyOSFamily.assert_called_once_with()
    SpyGraphicalImplementation.assert_called_once_with()
