from urist.artifact.version import Version


def test_df_version_vstring():
    df_version_string = "0.44.09"
    version = Version.from_str(df_version_string)
    assert version.vstring == df_version_string
