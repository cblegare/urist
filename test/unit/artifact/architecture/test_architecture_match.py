from collections import OrderedDict

import pytest

from urist.artifact.architecture import Architecture


def test_match(match_sample):
    reference_architecture, matched, a_match = match_sample
    assert reference_architecture.match(matched) is a_match


match_samples = OrderedDict(
    x86_and_x86_enum=(Architecture.X86, Architecture.X86, True),
    x86_and_x86_str=(Architecture.X86, "x86", True),
    x86_and_x86_str_upper=(Architecture.X86, "x86".upper(), True),
    x64_and_x64_enum=(Architecture.X64, Architecture.X64, True),
    x64_and_x64_str=(Architecture.X64, "x64", True),
    x64_and_x64_str_upper=(Architecture.X64, "x64".upper(), True),
    x86_and_x64_enum=(Architecture.X86, Architecture.X64, False),
    x86_and_x64_str=(Architecture.X86, "x64", False),
    x86_and_x64_str_upper=(Architecture.X86, "x64".upper(), False),
    x64_and_x86_enum=(Architecture.X64, Architecture.X86, False),
    x64_and_x86__str=(Architecture.X64, "x86", False),
    x64_and_x86__str_upper=(Architecture.X64, "x86".upper(), False),
    any_and_x64_enum=(Architecture.ANY, Architecture.X64, True),
    any_and_x64_str=(Architecture.ANY, "x64", True),
    any_and_x64_str_upper=(Architecture.ANY, "x64".upper(), True),
    any_and_x86_enum=(Architecture.ANY, Architecture.X86, True),
    any_and_x86__str=(Architecture.ANY, "x86", True),
    any_and_x86__str_upper=(Architecture.ANY, "x86".upper(), True),
)


@pytest.fixture(ids=list(match_samples.keys()), params=list(match_samples.values()))
def match_sample(request):
    return request.param
