import urist.compat
from urist.artifact.architecture import Architecture


def test_on_32bits(monkeypatch):
    monkeypatch.setattr(urist.compat, "bit32", True)
    monkeypatch.setattr(urist.compat, "bit64", False)

    assert Architecture.from_platform() is Architecture.X86


def test_on_64bits(monkeypatch):
    monkeypatch.setattr(urist.compat, "bit32", False)
    monkeypatch.setattr(urist.compat, "bit64", True)

    assert Architecture.from_platform() is Architecture.X64
