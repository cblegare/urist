from collections import OrderedDict

import pytest

from urist.artifact.graphics import GraphicalImplementation


def test_on_linux(monkeypatch):
    assert GraphicalImplementation.from_platform() is GraphicalImplementation.SDL


def test_match(match_sample):
    reference_architecture, matched, a_match = match_sample
    assert reference_architecture.match(matched) is a_match


match_samples = OrderedDict(
    legacy_and_legacy_enum=(
        GraphicalImplementation.LEGACY,
        GraphicalImplementation.LEGACY,
        True,
    ),
    legacy_and_legacy_str=(GraphicalImplementation.LEGACY, "legacy", True),
    legacy_and_legacy_str_upper=(
        GraphicalImplementation.LEGACY,
        "legacy".upper(),
        True,
    ),
    sdl_and_sdl_enum=(GraphicalImplementation.SDL, GraphicalImplementation.SDL, True,),
    sdl_and_sdl_str=(GraphicalImplementation.SDL, "sdl", True),
    sdl_and_sdl_str_upper=(GraphicalImplementation.SDL, "sdl".upper(), True),
    legacy_and_sdl_enum=(
        GraphicalImplementation.LEGACY,
        GraphicalImplementation.SDL,
        False,
    ),
    legacy_and_sdl_str=(GraphicalImplementation.LEGACY, "sdl", False),
    legacy_and_sdl_str_upper=(GraphicalImplementation.LEGACY, "sdl".upper(), False,),
    sdl_and_legacy_enum=(
        GraphicalImplementation.SDL,
        GraphicalImplementation.LEGACY,
        False,
    ),
    sdl_and_legacy__str=(GraphicalImplementation.SDL, "legacy", False),
    sdl_and_legacy__str_upper=(GraphicalImplementation.SDL, "legacy".upper(), False,),
    any_and_sdl_enum=(GraphicalImplementation.ANY, GraphicalImplementation.SDL, True,),
    any_and_sdl_str=(GraphicalImplementation.ANY, "sdl", True),
    any_and_sdl_str_upper=(GraphicalImplementation.ANY, "sdl".upper(), True),
    any_and_legacy_enum=(
        GraphicalImplementation.ANY,
        GraphicalImplementation.LEGACY,
        True,
    ),
    any_and_legacy__str=(GraphicalImplementation.ANY, "legacy", True),
    any_and_legacy__str_upper=(GraphicalImplementation.ANY, "legacy".upper(), True,),
)


@pytest.fixture(ids=list(match_samples.keys()), params=list(match_samples.values()))
def match_sample(request):
    return request.param
