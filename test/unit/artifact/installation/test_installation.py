from pathlib import Path
from unittest.mock import Mock

from urist.artifact.installation import Installation


def test_id_is_foldername():
    folder_name = "folder name"
    path = Path("parent", folder_name)

    installation = Installation(path, Mock())
    assert installation.id == folder_name


def test_add_insalls_component():
    path = Mock()
    component = Mock()

    installation = Installation(path, Mock())

    installation.add(component)
    component.install.assert_called_once_with(path)


def test_remove_uninsalls_component():
    path = Mock()
    component = Mock()

    installation = Installation(path, Mock())

    installation.remove(component)
    component.uninstall.assert_called_once_with(path)
