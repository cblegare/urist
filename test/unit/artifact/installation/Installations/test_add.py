from unittest.mock import Mock

import pytest

from urist.artifact.installation import (
    ExistingInstallFolderError,
    Installations,
)


def test_add_simply_creates_subdirectory():
    installation_id = "folder name"
    installation_path = Mock()
    installation_factory = Mock()
    installations_path = Mock(joinpath=Mock(return_value=installation_path))
    distributions = Mock()

    installations = Installations(
        installations_path, distributions, installation_factory
    )
    installations.add(installation_id)

    installations_path.joinpath.assert_called_once_with(installation_id)
    installation_path.mkdir.assert_called_once_with(parents=True, exist_ok=False)
    installation_factory.assert_called_once_with(installation_path, distributions)


def test_add_exising_installation_id_raises():
    installation_id = "exiting folder name"
    existing_installation_path = Mock(mkdir=Mock(side_effect=FileExistsError))
    installation_factory = Mock()
    installations_path = Mock(joinpath=Mock(return_value=existing_installation_path))
    distributions = Mock()

    with pytest.raises(ExistingInstallFolderError):
        installations = Installations(
            installations_path, distributions, installation_factory
        )
        installations.add(installation_id)

    installations_path.joinpath.assert_called_once_with(installation_id)
    existing_installation_path.mkdir.assert_called_once_with(
        parents=True, exist_ok=False
    )
    installation_factory.assert_not_called()
