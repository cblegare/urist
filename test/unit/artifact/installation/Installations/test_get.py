from unittest.mock import Mock, PropertyMock

import pytest

from urist.artifact.installation import (
    Installation,
    InstallationNotFound,
    Installations,
)


def test_get_looks_for_installation_id_in_found_installation(monkeypatch):
    distributions = Mock()
    installation_id = "exiting folder name"
    existing_installation_path = Mock(
        mkdir=Mock(side_effect=FileExistsError),
        name=PropertyMock(return_value=installation_id),
    )
    existing_installation = Installation(existing_installation_path, distributions)

    _installations = {installation_id: existing_installation}

    monkeypatch.setattr(Installations, "_installations", _installations)
    installations = Installations(Mock(), distributions, Mock())

    assert installations.get(installation_id) == existing_installation


def test_get_inexistant_raises(monkeypatch):
    monkeypatch.setattr(Installations, "_installations", {})
    installations = Installations(Mock(), Mock(), Mock())

    with pytest.raises(InstallationNotFound):
        installations.get("id of something we don't know about")
