from unittest.mock import Mock, PropertyMock

import pytest

from urist.artifact.installation import Installations


def test_folder_with_proper_init_file(tmp_path):
    basedir = tmp_path
    df_root = basedir / "Dwarf Fortress"
    init_file = df_root / "data" / "init" / "init.txt"
    init_file.parent.mkdir(parents=True)
    init_file.touch()

    assert Installations.looks_like_df_folder(df_root)


def test_empty_folder(tmp_path):
    assert Installations.looks_like_df_folder(tmp_path)


def test_folder_with_some_file(tmp_path):
    tmp_path.joinpath("some file").touch()
    assert not Installations.looks_like_df_folder(tmp_path)
