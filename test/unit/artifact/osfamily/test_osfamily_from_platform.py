import pytest

import urist.compat
from urist.artifact.osfamily import OSFamily


def test_on_windows(monkeypatch):
    monkeypatch.setattr(urist.compat, "windows", True)
    monkeypatch.setattr(urist.compat, "linux", False)
    monkeypatch.setattr(urist.compat, "osx", False)

    assert OSFamily.from_platform() is OSFamily.WINDOWS


def test_on_osx(monkeypatch):
    monkeypatch.setattr(urist.compat, "windows", False)
    monkeypatch.setattr(urist.compat, "linux", True)
    monkeypatch.setattr(urist.compat, "osx", False)

    assert OSFamily.from_platform() is OSFamily.OSX


def test_on_linux(monkeypatch):
    monkeypatch.setattr(urist.compat, "windows", False)
    monkeypatch.setattr(urist.compat, "linux", False)
    monkeypatch.setattr(urist.compat, "osx", True)

    assert OSFamily.from_platform() is OSFamily.LINUX


def test_on_something_unexpected(monkeypatch):
    monkeypatch.setattr(urist.compat, "windows", False)
    monkeypatch.setattr(urist.compat, "linux", False)
    monkeypatch.setattr(urist.compat, "osx", False)

    with pytest.raises(ValueError):
        OSFamily.from_platform()
