from collections import OrderedDict

import pytest

from urist.artifact.osfamily import OSFamily


def test_match(match_sample):
    reference_architecture, matched, a_match = match_sample
    assert reference_architecture.match(matched) is a_match


match_samples = OrderedDict(
    win_and_win_enum=(OSFamily.WINDOWS, OSFamily.WINDOWS, True),
    win_and_win_str=(OSFamily.WINDOWS, "win", True),
    win_and_win_str_upper=(OSFamily.WINDOWS, "win".upper(), True),
    osx_and_osx_enum=(OSFamily.OSX, OSFamily.OSX, True),
    osx_and_osx_str=(OSFamily.OSX, "osx", True),
    osx_and_osx_str_upper=(OSFamily.OSX, "osx".upper(), True),
    linux_and_linux_enum=(OSFamily.LINUX, OSFamily.LINUX, True),
    linux_and_linux_str=(OSFamily.LINUX, "linux", True),
    linux_and_linux_str_upper=(OSFamily.LINUX, "linux".upper(), True),
    win_and_osx_enum=(OSFamily.WINDOWS, OSFamily.OSX, False),
    win_and_osx_str=(OSFamily.WINDOWS, "osx", False),
    win_and_osx_str_upper=(OSFamily.WINDOWS, "osx".upper(), False),
    win_and_linux_enum=(OSFamily.WINDOWS, OSFamily.LINUX, False),
    win_and_linux_str=(OSFamily.WINDOWS, "linux", False),
    win_and_linux_str_upper=(OSFamily.WINDOWS, "linux".upper(), False),
    osx_and_win_enum=(OSFamily.OSX, OSFamily.WINDOWS, False),
    osx_and_win__str=(OSFamily.OSX, "win", False),
    osx_and_win__str_upper=(OSFamily.OSX, "win".upper(), False),
    osx_and_linux_enum=(OSFamily.OSX, OSFamily.LINUX, False),
    osx_and_linux__str=(OSFamily.OSX, "linux", False),
    osx_and_linux__str_upper=(OSFamily.OSX, "linux".upper(), False),
    linux_and_win_enum=(OSFamily.LINUX, OSFamily.WINDOWS, False),
    linux_and_win__str=(OSFamily.LINUX, "win", False),
    linux_and_win__str_upper=(OSFamily.LINUX, "win".upper(), False),
    linux_and_osx_enum=(OSFamily.LINUX, OSFamily.OSX, False),
    linux_and_osx__str=(OSFamily.LINUX, "osx", False),
    linux_and_osx__str_upper=(OSFamily.LINUX, "osx".upper(), False),
    any_and_win_enum=(OSFamily.ANY, OSFamily.WINDOWS, True),
    any_and_win__str=(OSFamily.ANY, "win", True),
    any_and_win__str_upper=(OSFamily.ANY, "win".upper(), True),
    any_and_osx_enum=(OSFamily.ANY, OSFamily.OSX, True),
    any_and_osx_str=(OSFamily.ANY, "osx", True),
    any_and_osx_str_upper=(OSFamily.ANY, "osx".upper(), True),
    any_and_linux_enum=(OSFamily.ANY, OSFamily.LINUX, True),
    any_and_linux_str=(OSFamily.ANY, "linux", True),
    any_and_linux_str_upper=(OSFamily.ANY, "linux".upper(), True),
)


@pytest.fixture(ids=list(match_samples.keys()), params=list(match_samples.values()))
def match_sample(request):
    return request.param
