from urist.artifact.component import Component, ComponentKind


def test_index_component_of_different_name_and_different_kind():
    dictionary = {
        Component("a name", ComponentKind.DF): object,
        Component("an other name", ComponentKind.DFHACK): object,
    }

    assert len(dictionary) == 2


def test_index_component_of_same_name_and_different_kind():
    same_name = "a name"
    dictionary = {
        Component(same_name, ComponentKind.DF): object,
        Component(same_name, ComponentKind.DFHACK): object,
    }

    assert len(dictionary) == 2


def test_index_component_of_different_name_and_same_kind():
    same_kind = ComponentKind.DF
    dictionary = {
        Component("a name", same_kind): object,
        Component("an other name", same_kind): object,
    }

    assert len(dictionary) == 2


def test_index_component_of_same_name_and_same_kind():
    same_name = "a name"
    same_kind = ComponentKind.DF
    dictionary = {
        Component(same_name, same_kind): object,
        Component(same_name, same_kind): object,
    }

    assert len(dictionary) == 1


def test_equality_component_of_different_name_and_different_kind():
    assert Component("a name", ComponentKind.DF) != Component(
        "an other name", ComponentKind.DFHACK
    )


def test_equality_component_of_same_name_and_different_kind():
    same_name = "a name"
    assert Component(same_name, ComponentKind.DF) != Component(
        same_name, ComponentKind.DFHACK
    )


def test_equality_component_of_different_name_and_same_kind():
    same_kind = ComponentKind.DF
    assert Component("a name", same_kind) != Component("an other name", same_kind)


def test_equality_component_of_same_name_and_same_kind():
    same_name = "a name"
    same_kind = ComponentKind.DF
    assert Component(same_name, same_kind) == Component(same_name, same_kind)
