import pytest
from click.testing import CliRunner

from urist import cli


def test_urist_provides_usage(cli_runner):
    result = cli_runner.invoke(cli.main)
    assert result.exit_code == 0, result.stderr
    assert "Usage: urist" in result.stdout, result.stdout


@pytest.fixture()
def cli_runner():
    runner = CliRunner(mix_stderr=False)
    return runner
