from sqlalchemy.orm import Query, Session, contains_eager

from urist.artifact.architecture import Architecture
from urist.artifact.component import ComponentKind
from urist.artifact.graphics import GraphicalImplementation
from urist.artifact.osfamily import OSFamily
from urist.db import KnownAsset, KnownComponent, KnownRelease


def test_df_component(dbsession: Session):
    query = dbsession.query(KnownComponent)
    result = query.filter(KnownComponent.kind == ComponentKind.DF).count()
    assert result == 1


def test_df_releases(dbsession: Session):
    query = dbsession.query(KnownRelease)
    result = query.filter(KnownComponent.kind == ComponentKind.DF).count()
    assert result == 164


def test_df_assets(dbsession: Session):
    query = dbsession.query(KnownAsset)
    result = query.filter(KnownComponent.kind == ComponentKind.DF).count()
    assert result == 910


def test_windows_32_sdl(dbsession: Session):
    query = dbsession.query(KnownAsset)
    only_windows: Query = query.filter(KnownAsset.osfamily == OSFamily.WINDOWS)
    only_32bits: Query = only_windows.filter(
        KnownAsset.architecture == Architecture.X86
    )
    only_sdl: Query = only_32bits.filter(
        KnownAsset.graphics == GraphicalImplementation.SDL
    )
    result = only_sdl.count()
    assert result == 114


def test_os_arch_graphics_version_gives_only_one(dbsession: Session):
    query: Query = dbsession.query(KnownAsset).join(KnownAsset.release).join(
        KnownRelease.component
    ).filter(
        KnownAsset.osfamily == OSFamily.WINDOWS,
        KnownAsset.architecture == Architecture.X64,
        KnownAsset.small == False,
        KnownAsset.graphics == GraphicalImplementation.SDL,
        KnownRelease.df_version == "0.44.12",
        KnownComponent.kind == ComponentKind.DF,
    )
    for elem in query.all():
        print(elem.archive_name, elem.__dict__)
    print(query.statement)
    result = query.count()
    assert result == 1
