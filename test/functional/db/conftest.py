import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from urist.db import setup_basedata, setup_schema, teardown_schema


@pytest.yield_fixture
def dbsession():
    """
    SQLAlchemy session with proper setup and teardown.
    """
    engine = create_engine("sqlite:///:memory:")
    setup_schema(engine)
    connection = engine.connect()
    # begin the nested transaction
    transaction = connection.begin()
    # use the connection with the already started transaction
    session = Session(bind=connection)
    setup_basedata(session)
    session.commit()

    yield session

    session.close()
    # roll back the broader transaction
    transaction.rollback()
    # put back the connection to the connection pool
    connection.close()
    teardown_schema(engine)
