import pytest

from urist.gui.app import make_window
from urist.gui.bridge import InstallationBridge


@pytest.fixture()
def urist_bridge(urist):
    bridge = InstallationBridge(urist)
    yield bridge


@pytest.fixture()
def window(qtbot, urist_bridge):
    window = make_window(urist_bridge)
    window.show()
    qtbot.addWidget(window)
    return window
