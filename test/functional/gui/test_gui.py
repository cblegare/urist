import pytest


@pytest.mark.gui
def test_window_title(window):
    assert window.windowTitle() == "Urist"


@pytest.mark.gui
def test_window_visible(window):
    assert window.isVisible()
