import os
from pathlib import Path

import pytest

from urist.app import Urist, make_urist
from urist.cfg import Env


@pytest.fixture()
def hostconfig(tmp_path):
    return Env(tmp_path / "config", tmp_path / "data", tmp_path / "cache")


@pytest.fixture()
def urist(hostconfig):
    return make_urist(hostconfig)


@pytest.fixture()
def project_root_dir():
    directory = Path(__file__).parent

    while directory:
        if list(directory.glob("setup.cfg")):
            return directory
        directory = directory.parent

    raise Exception("You broke something badly")


@pytest.fixture()
def folder_maker(tmpdir, file_maker):
    def maker(**lines_of_files):
        folder = Path(str(tmpdir))

        files = []

        for filename, lines in lines_of_files.items():
            filepath = file_maker(folder, filename, lines)
            files.append(filepath)
        return folder, files

    return maker


@pytest.fixture()
def file_maker():
    def maker(folder, filename, lines):
        filepath = Path(str(folder), filename)
        with filepath.open("w") as opened:
            opened.writelines(line + os.linesep for line in lines)

        return filepath

    return maker


@pytest.fixture()
def temp_dir(tmpdir):
    return Path(str(tmpdir))


def pytest_addoption(parser):
    parser.addoption(
        "--skipgui", action="store_true", default=False, help="Skip GUI tests"
    )


def pytest_configure(config):
    config.addinivalue_line("markers", "gui: mark test requiring a gui to run")


def pytest_collection_modifyitems(config, items):
    if not config.getoption("--skipgui"):
        # --skipgui given in cli: do not skip slow tests
        return
    skip_slow = pytest.mark.skip(reason="--skipgui received.")
    for item in items:
        if "gui" in item.keywords:
            item.add_marker(skip_slow)
