import inspect
from functools import reduce
from typing import Any, Callable, Dict, Iterator, Optional, Set, TypeVar

_Node = str
DependencyGraph = Dict[_Node, Set[_Node]]

_Factory = TypeVar("_Factory", bound=Callable[..., Any])


class ContainerBuilder:
    def __init__(self) -> None:
        self._service_dependencies: DependencyGraph = {}
        self._service_factories: Dict[_Node, Callable[..., Any]] = {}

    def build(self) -> Dict[_Node, Any]:
        services: Dict[_Node, Any] = {}

        for service_name in flat_topological_sort(self._service_dependencies):
            if service_name in services:
                continue
            factory = self._service_factories[service_name]
            dependencies = {
                name: services[name]
                for name in self._service_dependencies[service_name]
            }
            services[service_name] = factory(**dependencies)

        return services

    def register(self, func: _Factory, name: Optional[_Node] = None) -> _Factory:
        provides = name or func.__name__
        if provides == "<lambda>":
            raise RuntimeError(
                "You must provide a name when registering lambda factories"
            )
        depends = inspect.signature(func).parameters.keys()
        self._service_dependencies[provides] = set(depends)
        self._service_factories[provides] = func
        return func


def topological_sort(graph: DependencyGraph) -> Iterator[Set[_Node]]:
    """
    Make a topological sort of a directed acyclic graph using
    `Kahn's algorithm`_.

    Args:
        graph: A :class:`dict` of :class:`set` objects that maps a
            node to the set of its dependencies.

    Yields:
        Each yielded item is an independent set.  Elements of
        a given yielded set will only have dependencies on elements of
        previous sets.

    In case you require a flat generator as a result, you could wrap this
    function like the following:

    .. code-block:: python

        def flat_topological_sort(graph):
            for independent_set in topological_sort(graph):
                yield from independent_set
                # For a unique solution, sort the set first:
                # yield from sorted(independent_set)

    .. _Kahn's algorithm: https://en.wikipedia.org/wiki/Topological_sorting
    """
    if not graph:
        return []

    graph = _make_normalized_graph(graph)

    while True:
        nodes_without_deps = _find_nodes_without_deps(graph)

        if not nodes_without_deps:
            break

        yield nodes_without_deps

        graph = _make_graph_excluding_nodes(graph, nodes_without_deps)

    if len(graph):
        raise ValueError("Graph has cycles!")


def flat_topological_sort(graph: DependencyGraph) -> Iterator[_Node]:
    for independent_set in topological_sort(graph):
        yield from independent_set


def _find_nodes_without_deps(graph: DependencyGraph) -> Set[_Node]:
    return set(node for node, deps in graph.items() if not deps)


def _make_graph_excluding_nodes(
    graph: DependencyGraph, excluded_nodes: Set[_Node]
) -> DependencyGraph:
    return {
        node: set(deps - excluded_nodes)
        for node, deps in graph.items()
        if node not in excluded_nodes
    }


def _make_normalized_graph(graph: DependencyGraph) -> DependencyGraph:
    graph = graph.copy()

    for element, dependencies in graph.items():
        dependencies.discard(element)

    graph.update(
        {
            node_found_only_in_deps: set()
            for node_found_only_in_deps in reduce(set.union, graph.values())
            - set(graph.keys())
        }
    )

    return graph
