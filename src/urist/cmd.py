import logging
from typing import List, Optional

from urist import app
from urist.artifact.buildtype import BuildType, BuildTypeView
from urist.artifact.component import ComponentKind, ComponentView, Component
from urist.artifact.distribution import Distribution
from urist.artifact.version import Version
from urist.view import BaseView

log = logging.getLogger(__name__)


class Command:
    def __repr__(self):
        params = ", ".join(
            f"{key.lstrip('_')!s}={value!r}" for key, value in self.__dict__.items()
        )
        return (
            f"{self.__class__.__module__}" f".{self.__class__.__qualname__}({params!s})"
        )

    def execute(self, urist: "app.Urist") -> None:
        raise NotImplementedError()

    @property
    def view(self):
        raise NotImplementedError()


class NoOp(Command):
    class View(BaseView):
        pass

    def execute(self, urist: "app.Urist") -> None:
        pass

    @property
    def view(self):
        return self.View.from_obj(self)


class CreateInstall(Command):
    class View(BaseView):
        version: str
        build_type: BuildTypeView
        installation_id: str

        class Config:
            arbitrary_types_allowed = True

    def __init__(
        self,
        version: str,
        build_type: Optional[BuildType] = None,
        installation_id: Optional[str] = None,
    ):
        self.version = str(Version(version))
        self.build_type = build_type or BuildType.from_platform()
        self.installation_id = installation_id

    def execute(self, urist: "app.Urist") -> None:
        vanilla_df_distribution: Distribution = urist.distributions.find_one(
            kind=ComponentKind.DF,
            version=Version(self.version),
            buildtype=self.build_type,
        )
        installation_id = (
            self.installation_id
            or vanilla_df_distribution.archive_name.split(".", 1)[0]
        )

        installation = urist.installations.add(installation_id)
        installation.add(vanilla_df_distribution)

    @property
    def view(self):
        return self.View.from_obj(self)


class InstallComponent(Command):
    class View(BaseView):
        component: ComponentView
        build_type: BuildTypeView
        installation_id: str
        version: Optional[str]

        class Config:
            arbitrary_types_allowed = True

    def __init__(
            self,
            component: Component,
            installation_id: str,
            version: Optional[str] = None,
            build_type: Optional[BuildType] = None,
    ):
        self.version = str(Version(version)) if version else None
        self.component = component
        self.installation_id = installation_id
        self.build_type = build_type or BuildType.from_platform()

    def execute(self, urist: "app.Urist") -> None:
        installation = urist.installations.get(self.installation_id)
        distributions: List[Distribution] = urist.distributions.find_all(
            component=self.component,
            version=Version(self.version) if self.version else None,
            df_version=installation.components[ComponentKind.DF].release.version,
            buildtype=self.build_type,
        )

        distribution = sorted(
            distributions,
            key=lambda d: d.release.version,
            reverse=True
        )[0]

        installation.add(distribution)

    @property
    def view(self):
        return self.View.from_obj(self)


class Play(Command):
    class View(BaseView):
        installation_id: str

    def __init__(
        self, installation_id: Optional[str] = None,
    ):
        self.installation_id = installation_id

    def execute(self, urist: "app.Urist") -> None:
        installation = urist.installations.get(self.installation_id)
        if installation.user_config.enable_dfhack:
            installation.run(ComponentKind.DFHACK)
        else:
            installation.run(ComponentKind.DF)

    @property
    def view(self):
        return self.View.from_obj(self)


class SetSetting(Command):
    class View(BaseView):
        installation_id: str
        setting_id: str
        setting_value: str

    def __init__(self, installation_id: str, setting_id: str, setting_value: str):
        self.installation_id = installation_id
        self.setting_id = setting_id
        self.setting_value = setting_value

    def execute(self, urist: "app.Urist") -> None:
        installation = urist.installations.get(self.installation_id)
        installation.settings.set_value(self.setting_id, self.setting_value)

    @property
    def view(self):
        return self.View.from_obj(self)
