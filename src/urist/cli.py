import logging
from pathlib import Path
from typing import Any, List, Tuple, Union

import appdirs
import click

from urist import __dist__
from urist.app import Urist, make_urist, settings_definitions
from urist.artifact.component import ComponentKind, Component
from urist.artifact.architecture import Architecture
from urist.artifact.buildtype import BuildType
from urist.artifact.graphics import GraphicalImplementation
from urist.artifact.installation import InstallationNotFound
from urist.artifact.osfamily import OSFamily
from urist.cfg import Env, EnvView

logging.getLogger("sqlalchemy").setLevel(logging.DEBUG)
logging.getLogger("alembic").setLevel(logging.WARNING)
log = logging.getLogger(__name__)


default_dirs = appdirs.AppDirs(appname=__dist__.project_name)


def autocomplete_installs(ctx: click.Context, args: Any, incomplete: str) -> List[str]:
    return [install for install in ctx.obj.installations.all() if incomplete in install]


def get_default_install() -> Union[str, None]:
    urist: Urist = click.get_current_context().obj
    try:
        return urist.installations.get_default_installation().id
    except InstallationNotFound:
        return None


def autocomplete_settings(ctx: click.Context, args: Any, incomplete: str) -> List[str]:
    return [
        setting_id
        for setting_id in ctx.obj.settings_definitions
        if incomplete in setting_id
    ]


def get_setting_ids() -> List[str]:
    return list(settings_definitions().keys())


class MultilineLogger:
    def __init__(self, logger: logging.Logger = None):
        self._log = logger

    def debug(self, msg: str, *args, **kwargs):
        """
        Delegate a debug call to the underlying logger.
        """
        self.log(logging.DEBUG, msg, *args, **kwargs)

    def info(self, msg: str, *args, **kwargs):
        """
        Delegate an info call to the underlying logger.
        """
        self.log(logging.INFO, msg, *args, **kwargs)

    def warning(self, msg: str, *args, **kwargs):
        """
        Delegate a warning call to the underlying logger.
        """
        self.log(logging.WARNING, msg, *args, **kwargs)

    def error(self, msg: str, *args, **kwargs):
        """
        Delegate an error call to the underlying logger.
        """
        self.log(logging.ERROR, msg, *args, **kwargs)

    def exception(self, msg: str, *args, exc_info=True, **kwargs):
        """
        Delegate an exception call to the underlying logger.
        """
        self.log(logging.ERROR, msg, *args, exc_info=exc_info, **kwargs)

    def critical(self, msg: str, *args, **kwargs):
        """
        Delegate a critical call to the underlying logger.
        """
        self.log(logging.CRITICAL, msg, *args, **kwargs)

    def log(self, level: int, msg: str, *args, **kwargs):
        for line in msg.splitlines():
            self._log.log(level, line, *args, **kwargs)


log = MultilineLogger(log)

CONTEXT_SETTINGS = dict(help_option_names=["-h", "--help"], auto_envvar_prefix="URIST",)


@click.group(context_settings=CONTEXT_SETTINGS)
@click.version_option(version=__dist__.version)
@click.option(
    "-d",
    "--data-dir",
    default=default_dirs.user_data_dir,
    help="URIST installation folder.",
    show_default=True,
    show_envvar=True,
    allow_from_autoenv=True,
)
@click.option(
    "-c",
    "--cache-dir",
    default=default_dirs.user_cache_dir,
    help="URIST cache folder.",
    show_default=True,
    show_envvar=True,
    allow_from_autoenv=True,
)
@click.option(
    "-v",
    "--verbose",
    "verbosity",
    default=logging.WARNING / 10,
    help="Increase verbosity.",
    show_default=logging._levelToName[logging.WARNING],
    count=True,
)
@click.pass_context
def main(ctx: click.Context, data_dir: str, cache_dir: str, verbosity: int,) -> None:
    """\b
            .  .                   .        ▓▓▓▓▓▓▓▓▓▓▓▓
            |  |         o         |      ▓▓            ▓▓
            |  |   ;-.   .   ,-.   |-     ▓▓            ▓▓
            |  |   |     |   `-.   |      ▓▓  ██    ██  ▓▓
            `--`   '     '   `-'   `-'    ██            ██
                                          ████████████████
                                          ████  ▓▓▓▓  ████
                Helping newbs play        ████████████████
                    Dwarf Fortress          ████████████
                        since 2019              ██████
                                                  ████
        See below for subcommands.
        By default, 'gui' is launched.
    """
    logging.basicConfig(**logging_configuration(verbosity))

    host_config = Env(data_dir=Path(data_dir), cache_dir=Path(cache_dir),)

    log.debug("")
    log.debug(EnvView.from_obj(host_config).yaml())
    log.debug("Initiating Urist Object.")
    ctx.obj = make_urist(host_config)


@main.command()
@click.option(
    "-i",
    "--install",
    help="The installation to run.",
    default=get_default_install,
    show_default=True,
    autocompletion=autocomplete_installs,
)
@click.pass_obj
def play(obj: Urist, install: str) -> None:
    """
    Run Dwarf Fortress
    """
    from urist.cmd import Play

    cmd = Play(install)
    obj.do(cmd)


@main.command()
@click.option(
    "-i",
    "--install",
    help="The installation to configure.",
    default=get_default_install,
    show_default=True,
    autocompletion=autocomplete_installs,
)
@click.option(
    "-s",
    "--setting",
    "settings",
    help="Setting name and value to configure.",
    nargs=2,
    multiple=True,
    autocompletion=autocomplete_settings,
    type=(click.Choice(get_setting_ids()), str),
)
@click.pass_obj
def set_setting(obj: Urist, install, settings: List[Tuple[str, str]]) -> None:
    """
    Configure some setting in an installation
    """
    from urist.cmd import SetSetting

    for setting_id, value in settings:
        cmd = SetSetting(install, setting_id, value)
        obj.do(cmd)


@main.command()
@click.option(
    "-i",
    "--install",
    help="The installation to configure.",
    default=get_default_install,
    show_default=True,
    autocompletion=autocomplete_installs,
)
@click.option(
    "-s",
    "--setting",
    "setting_ids",
    help="Setting name and value to configure.",
    nargs=1,
    multiple=True,
    autocompletion=autocomplete_settings,
    type=click.Choice(get_setting_ids()),
)
@click.pass_obj
def get_setting(obj: Urist, install, setting_ids: List[str]) -> None:
    """
    Read the Game settings.
    """
    settings = obj.get_settings(install)
    setting_ids = setting_ids or settings.ids
    for setting_id in setting_ids:
        value = settings.get_value(setting_id)
        click.echo(f"{setting_id!s} = {value!s}")


@main.group()
def install() -> None:
    """Manipulate installations."""
    log.debug(f"Initiating CLI {click.get_current_context().info_name}.")


@install.command()
@click.option(
    "-i",
    "--install",
    help="The installation to configure.",
    default=get_default_install,
    show_default=True,
    autocompletion=autocomplete_installs,
)
@click.pass_obj
def describe(obj: Urist, install: str):
    """
    Display content of installation.
    """
    from urist.artifact.installation import InstallationView

    log.debug(f"Initiating CLI {click.get_current_context().info_name}.")

    installation = obj.installations.get(install)

    view = InstallationView.from_obj(installation)
    print(view.yaml())


@install.command("list")
@click.pass_obj
def list_installations(obj: Urist) -> None:
    """
    List available installations of Dwarf Fortress.
    """
    from urist.artifact.installation import InstallationsView

    view = InstallationsView.from_obj(obj.installations)

    print(view.yaml())


@install.command("add")
@click.pass_obj
@click.option(
    "--os-family",
    help="Select a specific OS family.",
    type=click.Choice([i for i in map(lambda x: str(x.value), OSFamily)]),
    default=OSFamily.from_platform().value,
    callback=lambda ctx, param, val: OSFamily(str(val)),
    show_default=True,
)
@click.option(
    "--arch",
    help="Select a specific architecture.",
    type=click.Choice([i for i in map(lambda x: str(x.value), Architecture)]),
    default=Architecture.from_platform().value,
    callback=lambda ctx, param, val: Architecture(str(val)),
    show_default=True,
)
@click.option(
    "--graphics",
    help="Select a specific graphical implementation.",
    type=click.Choice(
        [i for i in map(lambda x: str(x.value), GraphicalImplementation)]
    ),
    default=GraphicalImplementation.SDL.value,
    callback=lambda ctx, param, val: GraphicalImplementation(str(val)),
    show_default=True,
)
@click.option(
    "--name", help="Force a folder name for the installation.", default=None,
)
def add_installation(
    obj: Urist,
    version: str,
    os_family: OSFamily,
    arch: Architecture,
    graphics: GraphicalImplementation,
    name: str,
) -> None:
    """
    Install a vanilla Dwarf Fortress.
    """
    from urist.cmd import CreateInstall

    cmd = CreateInstall(version, BuildType(arch, os_family, graphics), name)
    obj.do(cmd)


@install.command("dfhack")
@click.option(
    "-i",
    "--install",
    "installation_id",
    help="The installation where to add a component.",
    default=get_default_install,
    show_default=True,
    autocompletion=autocomplete_installs,
)
@click.pass_obj
def install_dfhack(obj: Urist, installation_id: str):
    """
    Install DFHack.
    """
    from urist.cmd import InstallComponent
    cmd = InstallComponent(
        Component("DFHack", ComponentKind.DFHACK),
        installation_id=installation_id
    )
    obj.do(cmd)


@main.command()
@click.pass_obj
def gui(obj: Urist) -> None:
    """
    Run the Qutie Urist GUI!
    """
    import signal
    from urist.gui.app import make_gui

    signal.signal(signal.SIGINT, signal.SIG_DFL)

    gui = make_gui(obj)
    gui.start()


def logging_configuration(verbosity: int) -> Any:
    level = logging.CRITICAL - logging.DEBUG * (verbosity - 1)

    if level < logging.DEBUG:
        format = "%(levelname)8s %(name)-15s> %(message)s"
    else:
        format = "%(levelname)8s: %(message)s"

    return dict(
        level=level,
        format=format,
        handlers=[logging.FileHandler("stderr.log"), logging.StreamHandler()],
    )


if __name__ == "__main__":
    main(prog_name=__dist__.project_name)
