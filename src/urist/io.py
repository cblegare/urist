"""
Provides helper functions for manipulating strings and files.
"""
import hashlib
import io
import re
from pathlib import Path
from typing import Callable, Iterable, Optional, Union

import docutils.core
import in_place
from ruamel.yaml import YAML
from ruamel.yaml.compat import StringIO


class MyYAML(YAML):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.indent(mapping=2, sequence=12, offset=2)
        self.default_flow_style = False

    def dump(self, data, stream=None, **kw):
        inefficient = False
        if stream is None:
            inefficient = True
            stream = StringIO()
        super().dump(data, stream, **kw)
        if inefficient:
            return stream.getvalue()


yaml = MyYAML(typ="safe")


def snake_case(camel_case: str) -> str:
    words = re.findall(r"[A-Z]?[a-z]+|[A-Z]{2,}(?=[A-Z][a-z]|\d|\W|$)|\d+", camel_case)
    return "_".join(map(str.lower, words))


def replace(
    path: Union[Path, Iterable[Path]],
    search: str,
    replacement: str,
    replacer: Optional[Callable[[str, str, str], str]] = None,
    **open_kwargs: str
) -> None:
    """
    Replace lines of one or more file in place.

    Examples:

        >>> import tempfile
        >>> from pathlib import Path
        >>> import re
        >>> file = tempfile.NamedTemporaryFile()
        >>> path = Path(file.name)
        >>> content = "one\ntwo\nthree"
        >>> with path.open("w") as opened:
        >>>     opened.write(content)
        >>> replace(path, "one", "1")
        >>> path.read_text() == "1\ntwo\nthree"
        >>> replace(path, "(?P<groupname>two)", "\g<groupname> is 2", replacer=lambda text, search, replace: re.sub(search, replacement, text))
        >>> path.read_text() == "1\ntwo is 2\nthree"

    Args:
        path: Files to replace.  This can be one or multiple files.
        search: Search string.
        replacement: Replace string.
        replacer: Function to use to replace the text.  See Examples.
        **open_kwargs: Arguments forwarded to the file opener, including
            encoding.
    """
    if not isinstance(path, Iterable):
        paths = [path]
    else:
        paths = path

    def str_replace(text: str, old: str, new: str):
        return text.replace(old, new)

    replacer = replacer or str_replace

    for path in paths:
        with in_place.InPlace(name=path, **open_kwargs) as opened:
            for line in opened:
                opened.write(replacer(line, search, replacement))


def regex_replace(
    path: Union[Path, Iterable[Path]], regex: str, substitution: str, **open_kwargs: str
) -> None:
    """
    Convenient wrapper for :func:`urist.io.replace` using regexes.

    Args:
         path: Files to replace.  This can be one or multiple files.
        search: Search string.
        replacement: Replace string.
        **open_kwargs: Arguments forwarded to the file opener, including
            encoding.
    """

    def replacer(text: str, old: str, new: str):
        return re.sub(old, new, text)

    return replace(path, regex, substitution, replacer=replacer, **open_kwargs)


def rst_to_html(rst_string: str) -> str:
    """
    Renders RestructuredText to HTML.

    Args:
        rst_string: A RST string.

    Returns:
        Rendered HTML.
    """
    return docutils.core.publish_parts(rst_string, writer_name="html")["html_body"]


def hash_for_file(
    fileobj, algorithm: str = "sha1", block_size: int = io.DEFAULT_BUFFER_SIZE
):
    """
    Compute the checksum of a file object.

    Args:
        fileobj: File-like object.
        algorithm: Hashing algorithm to use.
        block_size: Read the file by chunks of this size.
    """
    if algorithm not in hashlib.algorithms_available:
        raise NameError('"{algorithm}" is not available.'.format(algorithm=algorithm))

    # According to hashlib documentation using new()
    # will be slower than calling using named
    # constructors, ex.: hashlib.md5()
    hasher = hashlib.new(algorithm)

    for chunk in iter(lambda: fileobj.read(block_size), b""):
        hasher.update(chunk)

    return hasher


def same_files(left: Path, right: Path) -> bool:
    with left.open("rb") as left_opened:
        left_sum = hash_for_file(left_opened).hexdigest()
    with right.open("rb") as right_opened:
        right_sum = hash_for_file(right_opened).hexdigest()

    return left_sum == right_sum
