import logging
from pathlib import Path
from typing import TYPE_CHECKING, Optional, cast

import alembic.command
import alembic.config
import alembic.migration
from pkg_resources import resource_filename
from sqlalchemy import (
    Boolean,
    Column,
    ForeignKey,
    Integer,
    String,
    UnicodeText,
    UniqueConstraint,
    create_engine,
)
from sqlalchemy.engine import Engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Query, Session, relationship, sessionmaker

from urist.artifact.architecture import Architecture
from urist.artifact.component import ComponentKind
from urist.artifact.graphics import GraphicalImplementation
from urist.artifact.osfamily import OSFamily
from urist.artifact.version import Version

if TYPE_CHECKING:
    # FIXME
    # workaround https://github.com/dropbox/sqlalchemy-stubs/issues/114
    from sqlalchemy.sql.type_api import TypeEngine
    from typing import TypeVar, Type

    _T = TypeVar("_T")

    class Enum(TypeEngine[_T]):
        def __init__(self, enum: Type[_T]) -> None:
            ...


else:
    from sqlalchemy import Enum


log = logging.getLogger(__name__)


Base = declarative_base()


class KnownComponent(Base):
    __tablename__ = "component"
    id = Column(Integer(), primary_key=True, autoincrement=True)
    name = Column("name", UnicodeText(), nullable=False)
    kind = Column("kind", Enum(ComponentKind), nullable=False)

    releases = relationship(
        "KnownRelease", order_by="KnownRelease.df_version", back_populates="component",
    )

    __table_args__ = (UniqueConstraint("name", "kind", name="component_name_kind_uc"),)


class KnownRelease(Base):
    __tablename__ = "release"
    id = Column(Integer(), primary_key=True, autoincrement=True)

    version = Column(String(50), nullable=False)
    df_version = Column(String(50))

    component_id = Column(Integer, ForeignKey("component.id"), nullable=False)
    component = relationship(
        "KnownComponent", back_populates="releases", cascade="save-update"
    )

    assets = relationship("KnownAsset", back_populates="release")

    __table_args__ = (UniqueConstraint("version", "component_id"),)

    def __str__(self):
        return f"{self.component.name} {self.version!s}"


class KnownAsset(Base):
    __tablename__ = "asset"
    id = Column(Integer(), primary_key=True, autoincrement=True)

    osfamily = Column(Enum(OSFamily), nullable=False)
    architecture = Column(Enum(Architecture), nullable=False, default=Architecture.X64)
    graphics = Column(
        Enum(GraphicalImplementation),
        nullable=False,
        default=GraphicalImplementation.SDL,
    )
    small = Column(Boolean(), nullable=False, default=False)

    url = Column(UnicodeText(), nullable=False, unique=True)

    release_id = Column(Integer, ForeignKey("release.id"))
    release = relationship(
        "KnownRelease", back_populates="assets", cascade="save-update"
    )

    @property
    def archive_name(self) -> str:
        if self.release.component.kind is ComponentKind.DF:
            name = ComponentKind.DF.value
        else:
            if self.release.component.name:
                name = "{name}_{kind}_for_df_{df_version}".format(
                    name=self.release.component.name,
                    kind=self.release.component.kind.value,
                    df_version=self.release.df_version,
                )
            else:
                name = "{kind}_for_df_{df_version}".format(
                    kind=self.release.component.kind.value,
                    df_version=self.release.df_version,
                )

        if self.url.endswith(".zip"):
            extension = "zip"
        elif self.url.endswith(".deb"):
            extension = "deb"
        elif self.url.endswith(".tar.gz"):
            extension = "tar.gz"
        elif self.url.endswith(".7z"):
            extension = "7z"
        elif self.url.endswith(".tar.bz2"):
            extension = "tar.bz2"
        elif self.url.endswith(".bz2"):
            extension = "bz2"
        else:
            raise Exception(self.url)

        return (
            "{name}-{version}"
            "-{osfamily}-{architecture}-{graphics}"
            ".{extension}".format(
                name=name,
                version=self.release.version,
                osfamily=self.osfamily.value,
                architecture=self.architecture.value,
                graphics=self.graphics.value,
                extension=extension,
            )
        )


def setup_schema(engine: Engine) -> None:
    Base.metadata.create_all(engine)


def teardown_schema(engine: Engine) -> None:
    Base.metadata.drop_all(engine)


def setup_basedata(session: Session) -> None:
    import json

    builds_filename = resource_filename("urist.db", "db.json")
    with open(builds_filename) as opened:
        inventory = json.load(opened)

    component_index = {}

    for raw_component in inventory["components"].values():
        component = KnownComponent(
            name=raw_component["name"], kind=ComponentKind(raw_component["kind"]),
        )
        session.add(component)
        component_index[raw_component["kind"]] = component

    for raw_release in inventory["releases"]:
        release = KnownRelease(
            version=raw_release["version"],
            df_version=raw_release["df_version"],
            component=component_index[raw_release["kind"]],
        )
        session.add(release)
        for raw_asset in raw_release["assets"]:
            asset = KnownAsset(
                osfamily=OSFamily(raw_asset["osfamily"]),
                architecture=Architecture(raw_asset["architecture"]),
                graphics=GraphicalImplementation(raw_asset["graphics"]),
                url=raw_asset["download_url"],
                small=raw_asset["small"],
                release=release,
            )
            session.add(asset)


class Database:
    def __init__(self, connection_string: str):
        log.info(f"Creating database service for '{connection_string}'.")
        self.uri = connection_string
        self._engine = create_engine(connection_string, echo=False)
        self._Session = sessionmaker(bind=self._engine)
        self._session = cast(Session, self._Session())

    @property
    def revision(self) -> str:
        with self.engine.begin() as connection:
            context = alembic.migration.MigrationContext.configure(connection)
            current_rev = context.get_current_revision()

        if not isinstance(current_rev, str):
            current_rev = "base"

        return str(current_rev)

    def migrate(self, revision: str = "head") -> None:
        script_location = Path(__file__).parent.joinpath("migration")

        alembic_cfg = alembic.config.Config()
        alembic_cfg.set_main_option("script_location", str(script_location))

        log.debug("using database at '{!s}'".format(self.engine.url))
        log.debug("using migration at '{!s}'".format(script_location))
        log.info("upgrading database to '{!s}'".format(revision))

        with self.engine.begin() as connection:
            alembic_cfg.attributes["connection"] = connection
            alembic.command.upgrade(alembic_cfg, revision)

    def downgrade(self, revision: str) -> None:
        script_location = Path(__file__).parent.joinpath("migration")

        alembic_cfg = alembic.config.Config()
        alembic_cfg.set_main_option("script_location", str(script_location))

        log.debug("using database at '{!s}'".format(self.engine.url))
        log.debug("using migration at '{!s}'".format(script_location))
        log.info("downgrading database to '{!s}'".format(revision))

        with self.engine.begin() as connection:
            alembic_cfg.attributes["connection"] = connection
            alembic.command.downgrade(alembic_cfg, revision)

    @property
    def engine(self) -> Engine:
        return self._engine

    @property
    def session(self) -> Session:
        return self._session

    def query_assets(
        self,
        small: bool = False,
        graphics: GraphicalImplementation = GraphicalImplementation.SDL,
        kind: Optional[ComponentKind] = None,
        osfamily: Optional[OSFamily] = None,
        arch: Optional[Architecture] = None,
        version: Optional[Version] = None,
        df_version: Optional[str] = None,
    ) -> Query:
        query: Query = self.session.query(KnownAsset).join(KnownAsset.release).join(
            KnownRelease.component
        ).filter(KnownAsset.small == small)

        if kind is not None:
            query = query.filter(KnownComponent.kind == kind)
        if graphics is not GraphicalImplementation.ANY:
            query = query.filter(KnownAsset.graphics == graphics)
        if osfamily is not None and osfamily is not OSFamily.ANY:
            query = query.filter(KnownAsset.osfamily == osfamily)
        if arch is not None and arch is not Architecture.ANY:
            query = query.filter(KnownAsset.architecture == arch)
        if df_version is not None:
            query = query.filter(KnownRelease.df_version == df_version)
        if version is not None:
            query = query.filter(KnownRelease.version == version.vstring)

        return query
