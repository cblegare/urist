from alembic import context
from sqlalchemy import engine_from_config, pool

from urist import db
from urist.db import KnownAsset, KnownComponent, KnownRelease

target_metadata = db.Base.metadata


config = context.config


def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    from urist import db
    from urist.db import KnownAsset, KnownRelease, KnownComponent

    target_metadata = db.Base.metadata
    # db_object = db.Database()
    # url = db_object.uri
    context.configure(
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    from urist import db
    from urist.db import KnownAsset, KnownRelease, KnownComponent

    target_metadata = db.Base.metadata

    # db_object = db.Database()
    # connectable = db_object._engine

    connection = config.attributes["connection"]
    with connection.connect() as connection:
        context.configure(connection=connection, target_metadata=target_metadata)

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
