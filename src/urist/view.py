from pydantic import BaseModel

from urist.io import snake_case, yaml


class BaseView(BaseModel):
    class Config:
        orm_mode = True

    @classmethod
    def from_obj(cls, obj):
        obj_classname = obj.__class__.__name__
        view = cls.from_orm(obj)
        view.__config__.classname = obj_classname
        return view

    def yaml(self) -> str:
        root_key = snake_case(self.__config__.classname)
        data_view = {root_key: {}}
        data_view[root_key].update(yaml.load(self.json()))

        return yaml.dump(data_view)
