import urllib.parse
import urllib.request
from pathlib import Path
from typing import Optional, Type

from urist import UristError
from urist.artifact.distribution import Distribution


class Build:
    def __init__(self, path: Path, distribution: Distribution):
        """
        Represent a concrete Dwarf Fortress compoent build file.

        Args:
            path: Filesystem path where the build archive is located. This
                can also be created using the parent directory path, and the
                filename will be inferred from the distribution.
            distribution: Dwarf Fortress distribution for this build.
        """
        if distribution.archive_name == path.name:
            path = path.parent

        self._path = path / distribution.archive_name
        self._distribution = distribution

    @property
    def path(self) -> Path:
        return self._path

    @property
    def distribution(self) -> Distribution:
        return self._distribution


class BuildFactory:
    def __init__(self, cls: Optional[Type[Build]] = None):
        self._cls = cls or Build

    def __call__(self, *args, **kwargs) -> Build:
        return self._cls(*args, **kwargs)


class Builds:
    def __init__(self, storage: Path, factory: Optional[BuildFactory] = None):
        self._path = storage
        self._factory = factory or BuildFactory()

    def get(self, component: Distribution) -> Build:
        archive_path = self._path / component.archive_name
        if not archive_path.exists():
            build = self.add(component)
        else:
            build = self._factory(archive_path, component)

        return build

    def add(self, component: Distribution) -> Build:
        archive_path = self._path / component.archive_name
        self._download(component.download_url, archive_path)
        return self._factory(archive_path, component)

    def _download(self, url: str, destination_path: Path) -> None:  # nosec
        if urllib.parse.urlparse(url).scheme not in ["http", "https"]:
            # Mitigated Bandit B310 with this and added  # nosec
            raise UristError("URL doesn't look secure: '{!s}'".format(url))

        destination_path.parent.mkdir(parents=True, exist_ok=True)

        urllib.request.urlretrieve(url, filename=str(destination_path))
