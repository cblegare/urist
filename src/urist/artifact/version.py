from distutils.version import LooseVersion
from typing import Type, TypeVar

# Hide the class name in this module
# so it is easier to change the implementation
_Version = LooseVersion


T = TypeVar("T", bound="Version")


class Version(_Version):
    @classmethod
    def from_str(cls: Type[T], version_string: str) -> T:
        return cls(version_string)

    def __hash__(self):
        return hash(str(self))
