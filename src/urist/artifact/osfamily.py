from enum import Enum
from typing import Type, TypeVar, Union

import urist.compat

T = TypeVar("T", bound="OSFamily")


class OSFamily(Enum):
    WINDOWS = "win"
    LINUX = "linux"
    OSX = "osx"
    ANY = "any"

    @classmethod
    def get_choices(cls):
        default = cls.from_platform()
        others = {cls.WINDOWS, cls.LINUX, cls.OSX}
        others.remove(default)
        choices = [default]
        choices.extend(others)
        return choices

    @classmethod
    def from_platform(cls: Type[T]) -> "OSFamily":
        if urist.compat.windows:
            instance = cls.WINDOWS
        elif urist.compat.osx:
            instance = cls.OSX
        elif urist.compat.linux:
            instance = cls.LINUX
        else:
            raise ValueError()
        return instance

    def match(self, other: Union["OSFamily", str]) -> bool:
        if isinstance(other, str):
            other = OSFamily(other.lower())
        if OSFamily.ANY in (other, self):
            return True
        else:
            return self is other
