from enum import Enum
from typing import Type, TypeVar, Union

import urist.compat

T = TypeVar("T", bound="Architecture")


class Architecture(str, Enum):
    X86 = "x86"
    X64 = "x64"
    ANY = "any"

    @classmethod
    def get_choices(cls):
        default = cls.from_platform()
        others = {cls.X86, cls.X64}
        others.remove(default)
        choices = [default]
        choices.extend(others)
        return choices

    @classmethod
    def from_platform(cls: Type[T]) -> "Architecture":
        instance = cls.X64 if urist.compat.bit64 else cls.X86
        return instance

    def match(self, other: Union["Architecture", str]) -> bool:
        if isinstance(other, str):
            other = Architecture(other.lower())
        if Architecture.ANY in (other, self):
            return True
        else:
            return self is other
