from enum import Enum
from typing import Type, TypeVar, Union

T = TypeVar("T", bound="GraphicalImplementation")


class GraphicalImplementation(Enum):
    SDL = "sdl"
    LEGACY = "legacy"
    ANY = "any"

    @classmethod
    def get_choices(cls):
        return [cls.SDL, cls.LEGACY]

    @classmethod
    def from_platform(cls: Type[T]) -> "GraphicalImplementation":
        return cls.SDL

    def match(self, other: Union["GraphicalImplementation", str]) -> bool:
        if isinstance(other, str):
            other = GraphicalImplementation(other.lower())
        if GraphicalImplementation.ANY in (other, self):
            return True
        else:
            return self is other
