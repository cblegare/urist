from typing import Optional, Type, TypeVar

from urist import UristError
from urist.artifact.architecture import Architecture
from urist.artifact.graphics import GraphicalImplementation
from urist.artifact.osfamily import OSFamily
from urist.compat import HashableMixin
from urist.view import BaseView

T = TypeVar("T", bound="BuildType")


class UnknownBuildType(UristError):
    pass


class BuildType(HashableMixin):
    def __init__(
        self,
        architecture: Optional[Architecture] = None,
        osfamily: Optional[OSFamily] = None,
        graphics: Optional[GraphicalImplementation] = None,
    ):
        self._architecture = architecture or Architecture.from_platform()
        self._osfamily = osfamily or OSFamily.from_platform()
        self._graphics = graphics or GraphicalImplementation.from_platform()

    @property
    def architecture(self) -> Architecture:
        return self._architecture

    @property
    def osfamily(self) -> OSFamily:
        return self._osfamily

    @property
    def graphics(self) -> GraphicalImplementation:
        return self._graphics

    def match(self, other: "BuildType") -> bool:
        if not isinstance(other, BuildType):
            return False
        return (
            self.architecture.match(other.architecture)
            and self.osfamily.match(other.osfamily)
            and self.graphics.match(other.graphics)
        )

    @classmethod
    def from_platform(cls: Type[T]) -> T:
        return cls(
            architecture=Architecture.from_platform(),
            osfamily=OSFamily.from_platform(),
            graphics=GraphicalImplementation.from_platform(),
        )


class BuildTypeView(BaseView):
    architecture: Architecture
    osfamily: OSFamily
    graphics: GraphicalImplementation
