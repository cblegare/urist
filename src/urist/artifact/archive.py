import shutil
import tarfile
import zipfile
from enum import Enum
from pathlib import Path
from typing import List, Optional


class UnsupportedArchiveException(RuntimeError):
    pass


class _ArchiveType(Enum):
    ZIP = "zip"
    TARBZ2 = "tar.bz2"

    @property
    def file_extension(self) -> str:
        return ".{}".format(self.value)

    @classmethod
    def from_path(cls, path: Path) -> "_ArchiveType":
        """
        Construct from an archive path.

        Args:
            path : Use this archive to construct this.

        Returns:
            A proper archive type guessed from given path.
        """
        if path.name.endswith(cls.ZIP.value):
            return cls.ZIP
        elif path.name.endswith(cls.TARBZ2.value):
            return cls.TARBZ2
        else:
            raise UnsupportedArchiveException(path)

    def extract_all(self, source_file: Path, destination_folder: Path) -> List[Path]:
        """
        Extract a source archive into a folder.

        Args:
            source_file: Archive path to extract.
            destination_folder: Destination folder in which to extract
                archive content.
        """
        extractor = self._extract_all_tarbz2

        if self is self.__class__.ZIP:
            extractor = self._extract_all_zip

        members = [
            Path(destination_folder, member)
            for member in extractor(source_file, destination_folder)
        ]

        toplevel_files = list(destination_folder.iterdir())

        if len(toplevel_files) == 1 and toplevel_files[0].is_dir():
            toplevel_dir = toplevel_files[0]

            for innerfile in toplevel_dir.rglob("*"):
                shutil.move(str(innerfile), str(toplevel_dir.parent))
            toplevel_dir.rmdir()

            members = [member.relative_to(toplevel_dir) for member in members]

        return [destination_folder.joinpath(member) for member in members]

    def _extract_all_zip(
        self, source_file: Path, destination_folder: Path
    ) -> List[str]:
        zip_object = zipfile.ZipFile(source_file)

        members = zip_object.namelist()

        zip_object.extractall(destination_folder, members=members)

        return members

    def _extract_all_tarbz2(
        self, source_file: Path, destination_folder: Path
    ) -> List[str]:
        tar_object = tarfile.open(source_file)

        members = tar_object.getmembers()

        tar_object.extractall(destination_folder, members)

        return [member.name for member in members]


class Archive:
    def __init__(self, path: Path, archive_type: Optional[_ArchiveType] = None):
        """
        Wrap different implementations of archive files.

        Args:
            pathL Filesystem path to the archive.
            archive_type: Force a specific archive type
                (in case the path extension would be ambiguous).
        """
        self._path = path
        self._type = archive_type or _ArchiveType.from_path(self._path)

    def extract(self, destination_folder: Path) -> List[Path]:
        return self._type.extract_all(self._path, destination_folder)
