from pathlib import Path
from typing import Optional, Type

from pydantic import validator

from urist.artifact.buildtype import BuildType, BuildTypeView
from urist.artifact.version import Version
from urist.compat import HashableMixin
from urist.view import BaseView


class Release(HashableMixin):
    def __init__(self, build_type: BuildType, version: Version):
        """
        Represent distribution of some software or extension.

        Args:
            build_type: Compile for build type.
            version: Released version.
        """
        self._type = build_type
        self._version = version

    @property
    def version(self) -> Version:
        return self._version

    @property
    def buildtype(self) -> BuildType:
        return self._type


class ReleaseFactory:
    def __init__(self, cls: Optional[Type[Release]] = None):
        self._cls = cls or Release

    def __call__(self, build_type: BuildType, version: Version) -> Release:
        """
        Mimic the managed class constructor.
        """
        return self._cls(build_type, version)

    def from_archive_name(self, archive_name: str) -> Release:
        """
        Construct a distribution from a build archive filename.

        The build type and version are inferred from the filename.

        Args:
            archive_name: The build archive file name. Passing a full path
                may not work here.

        Returns:
            A fully retro-constructed distribution.
        """
        raise NotImplementedError()

    def from_installation_folder(self, path: Path) -> Release:
        """
        Construct a distribution from an installation.

        Args:
            path: Folder where Dwarf Fortress is installed.

        Returns:
            A fully retro-constructed distribution
        """
        raise NotImplementedError()


class ReleaseView(BaseView):
    buildtype: BuildTypeView
    version: str

    @validator("version", pre=True)
    def cast_version(cls, v):
        return str(v)
