from enum import Enum

from urist.compat import HashableMixin
from urist.view import BaseView


class ComponentKind(str, Enum):
    DF = "df"
    DFHACK = "dfhack"
    COLOR = "color"
    DEFAULTS = "defaults"
    EMBARK = "embark"
    EXTRA = "extra"
    GRAPHIC = "graphic"
    KEYBIND = "keybind"
    MODS = "mods"
    TILESET = "tileset"
    UTILITY = "utility"
    DFHACK_PLUGIN = "dfhack_plugin"
    DFHACK_SCRIPT = "dfhack_script"


class Component(HashableMixin):
    def __init__(self, name: str, kind: ComponentKind):
        self._name = name
        self._kind = kind

    @property
    def name(self) -> str:
        return self._name

    @property
    def kind(self) -> ComponentKind:
        return self._kind


class ComponentView(BaseView):
    name: str
    kind: ComponentKind
