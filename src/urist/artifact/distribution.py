import logging
from pathlib import Path
from typing import Dict, List, Mapping, Optional, Type, TypeVar

import sqlalchemy.orm

import urist.db
from urist import UristError
from urist.artifact.architecture import Architecture
from urist.artifact.buildtype import BuildType
from urist.artifact.component import Component, ComponentKind, ComponentView
from urist.artifact.graphics import GraphicalImplementation
from urist.artifact.osfamily import OSFamily
from urist.artifact.release import Release, ReleaseView
from urist.artifact.version import Version
from urist.compat import HashableMixin
from urist.view import BaseView

# from urist.db import Database, KnownAsset

log = logging.getLogger(__name__)


T = TypeVar("T", bound="Distribution")


class DistributionNotFound(UristError):
    pass


class DistributionView(BaseView):
    component: ComponentView
    release: ReleaseView


class Distribution(HashableMixin):
    def __init__(
        self, component: Component, release: Release, archive_name: str, url: str,
    ):
        self._component = component
        self._release = release
        self._archive_name = archive_name
        self._url = url

    def install(self, path: Path) -> None:
        raise NotImplementedError()

    def uninstall(self, path: Path) -> None:
        raise NotImplementedError()

    def is_installed_at(self, path: Path) -> bool:
        raise NotImplementedError()

    @property
    def kind(self):
        return self._component.kind

    @property
    def component(self):
        return self._component

    @property
    def release(self):
        return self._release

    @property
    def archive_name(self) -> str:
        return self._archive_name

    @property
    def download_url(self) -> str:
        return self._url

    @property
    def executable(self) -> Path:
        raise NotImplementedError()


class DistributionFactory:
    KIND = NotImplementedError()

    def create_from_orm(self, model: "urist.db.KnownAsset") -> Distribution:
        raise NotImplementedError()

    def create_from_path(self, path: Path) -> Distribution:
        raise NotImplementedError()


class Distributions:
    def __init__(
        self,
        database: "urist.db.Database",
        factories: Mapping[ComponentKind, DistributionFactory],
    ):
        self._db = database
        self._factories = factories

    def list_from_path(self, path: Path) -> List[Distribution]:
        found = []
        for factory in self._factories.values():
            try:
                distribution = factory.create_from_path(path)
            except DistributionNotFound as error:
                log.debug(
                    "No distribution was found for kind {!s}".format(factory.KIND)
                )
            else:
                found.append(distribution)

        return found

    def find_all(
        self,
        small: bool = False,
        graphics: GraphicalImplementation = GraphicalImplementation.SDL,
        kind: Optional[ComponentKind] = None,
        osfamily: Optional[OSFamily] = None,
        arch: Optional[Architecture] = None,
        df_version: Optional[str] = None,
        version: Optional[Version] = None,
        component: Optional[Component] = None,
        release: Optional[Release] = None,
        buildtype: Optional[BuildType] = None,
    ) -> List[Distribution]:
        return [
            self._factories[model.release.component.kind].create_from_orm(model)
            for model in self._build_query(
                small=small,
                graphics=graphics,
                kind=kind,
                osfamily=osfamily,
                arch=arch,
                df_version=df_version,
                version=version,
                component=component,
                release=release,
                buildtype=buildtype,
            ).all()  # type: ignore
        ]

    def find_one(
        self,
        small: bool = False,
        graphics: GraphicalImplementation = GraphicalImplementation.SDL,
        kind: Optional[ComponentKind] = None,
        osfamily: Optional[OSFamily] = None,
        arch: Optional[Architecture] = None,
        df_version: Optional[str] = None,
        version: Optional[Version] = None,
        component: Optional[Component] = None,
        release: Optional[Release] = None,
        buildtype: Optional[BuildType] = None,
    ) -> Distribution:
        try:
            query = self._build_query(
                small=small,
                graphics=graphics,
                kind=kind,
                osfamily=osfamily,
                arch=arch,
                df_version=df_version,
                version=version,
                component=component,
                release=release,
                buildtype=buildtype,
            )
            model = query.one()
            distribution = self._factories[
                model.release.component.kind
            ].create_from_orm(
                model  # type: ignore
            )
        except sqlalchemy.exc.SQLAlchemyError as error:
            raise DistributionNotFound(str(query)) from error
        else:
            return distribution

    def _build_query(
        self,
        small: Optional[bool] = None,
        graphics: Optional[GraphicalImplementation] = None,
        kind: Optional[ComponentKind] = None,
        osfamily: Optional[OSFamily] = None,
        arch: Optional[Architecture] = None,
        df_version: Optional[str] = None,
        version: Optional[Version] = None,
        component: Optional[Component] = None,
        release: Optional[Release] = None,
        buildtype: Optional[BuildType] = None,
    ) -> sqlalchemy.orm.Query:
        canonical_args = {
            "small": small,
            "graphics": graphics,
            "kind": kind,
            "osfamily": osfamily,
            "arch": arch,
            "version": version,
            "df_version": df_version,
        }
        if release is not None:
            buildtype = release.buildtype
            canonical_args["version"] = release.version
        if buildtype is not None:
            canonical_args["graphics"] = buildtype.graphics
            canonical_args["osfamily"] = buildtype.osfamily
            canonical_args["arch"] = buildtype.architecture
        if component is not None:
            canonical_args["kind"] = component.kind

        if canonical_args["version"] is not None:
            canonical_args["version"] = str(canonical_args["version"])

        if canonical_args["df_version"] is not None:
            canonical_args["df_version"] = str(canonical_args["df_version"])

        provided_canonical_args = {
            name: value for name, value in canonical_args.items() if value is not None
        }

        query = self._db.query_assets(**provided_canonical_args)
        return query
