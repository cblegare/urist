import re
import shutil
from pathlib import Path
from typing import Any, Iterable, Mapping, Tuple, Union

from urist import UristError
from urist.artifact import installation
from urist.artifact.component import ComponentKind
from urist.artifact.osfamily import OSFamily
from urist.io import regex_replace, replace, same_files, yaml
from urist.view import BaseView

SettingID = str

OneOrMorePath = Union[Path, Iterable[Path]]

PyValue = Union[str, Any]


RAWFILE_ENCODING = "cp437"


class InvalidRawValue(UristError):
    pass


class ValidationError(UristError):
    pass


class SettingDefinition:
    def read(self, installation: "installation.Installation") -> Any:
        raise NotImplementedError()

    def write(self, value: PyValue, installation: "installation.Installation") -> None:
        raise NotImplementedError()

    def help(self, installation: "installation.Installation") -> str:
        raise NotImplementedError(self.__class__)


class Settings:
    def __init__(
        self,
        installation: "installation.Installation",
        setting_definitions: Mapping[SettingID, SettingDefinition],
    ):
        self._installation = installation
        self._definitions = setting_definitions

    @property
    def ids(self) -> Iterable[str]:
        return self._definitions.keys()

    def get_help(self, setting_id: SettingID) -> str:
        definition = self._definitions[setting_id]
        help_msg = definition.help(self._installation)
        return help_msg

    def get_value(self, setting_id: SettingID) -> Any:
        definition = self._definitions[setting_id]
        value = definition.read(self._installation)
        return value

    def set_value(self, setting_id: SettingID, value: PyValue) -> None:
        definition = self._definitions[setting_id]
        definition.write(self._installation, value)


class ClassDocstringHelpMixin:
    def help(self, installation: "installation.Installation") -> str:
        return self.__class__.__doc__.strip()


class InitFileMixin:
    def _file(self, installation_root: Path) -> Union[Path, Iterable[Path]]:
        return installation_root.joinpath("data/init/init.txt")


class DInitFileMixin:
    def _file(self, installation_root: Path) -> Union[Path, Iterable[Path]]:
        return installation_root.joinpath("data/init/d_init.txt")


class Bool:
    pass


class Choice:
    pass


class Integer:
    pass


class BoolMixin:
    def _raw_to_py(self, raw_value: str) -> bool:
        if raw_value == "YES":
            return True
        elif raw_value == "NO":
            return False
        else:
            raise InvalidRawValue(f"Expected 'YES' or 'NO', got '{raw_value!s}'.")

    def _py_to_raw(self, py_value: PyValue) -> str:
        if py_value is True:
            return "YES"
        elif py_value is False:
            return "NO"
        elif isinstance(py_value, str) and py_value.lower() == "true":
            return "YES"
        elif isinstance(py_value, str) and py_value.lower() == "false":
            return "NO"
        else:
            raise ValidationError(f"Expected boolean, got {type(py_value)!s}.")


class IntegerMixin:
    def _raw_to_py(self, raw_value: str) -> int:
        if str(int(raw_value)) != raw_value:
            raise InvalidRawValue(f"Expected integer, got {raw_value!s}.")

        return int(raw_value)

    def _py_to_raw(self, py_value: int) -> str:
        if int(str(py_value)) != py_value:
            raise ValidationError(f"Expected integer, got {type(py_value)!s}.")

        return str(py_value)


class StringChoiceMixin:
    def _raw_to_py(self, raw_value: str) -> str:
        normalized = str(raw_value).upper()

        if normalized not in self.choices:
            raise InvalidRawValue(
                f"Expected one of {self.choices!s}, got '{raw_value!s}."
            )

        return normalized

    def _py_to_raw(self, py_value: str) -> str:
        normalized = str(py_value).upper()

        if normalized not in self.choices:
            raise ValidationError(
                f"Expected one of {self.choices!s}, got '{py_value!s}."
            )

        return normalized

    @property
    def choices(self) -> Iterable[str]:
        raise NotImplementedError


class SimpleTextReplaceSetting(SettingDefinition):
    def read(self, installation: "installation.Installation") -> Any:
        raw_file = self._file(installation.path)

        match = re.match(
            rf".*\[{self._token_key!s}:(?P<value>[a-zA-Z0-9:]+)\].*",
            raw_file.read_text(encoding=RAWFILE_ENCODING),
            flags=re.M | re.S,
        )

        if match:
            value = self._raw_to_py(match.group("value"))
            return value
        else:
            raise InvalidRawValue(
                f"Could not find a raw token for " f"{self._token_key} in {raw_file}"
            )

    def write(self, installation: "installation.Installation", value: PyValue) -> None:
        raw_value = self._py_to_raw(value)
        raw_file = self._file(installation.path)

        regex_replace(
            raw_file,
            rf"(?P<before>.*)"
            rf"\[{self._token_key!s}:[a-zA-Z0-9:]+\]"
            rf"(?P<after>.*)",
            rf"\g<before>[{self._token_key!s}:{raw_value!s}]\g<after>",
            encoding=RAWFILE_ENCODING,
        )

    @property
    def _token_key(self) -> str:
        raise NotImplementedError()

    def _raw_to_py(self, raw_value: str) -> Any:
        raise NotImplementedError()

    def _py_to_raw(self, py_value: PyValue) -> str:
        raise NotImplementedError()

    def _file(self, installation_path: Path) -> Path:
        raise NotImplementedError()


class EnableAquifer(ClassDocstringHelpMixin, Bool, SettingDefinition):
    """
    Enable the presence of aquifers in any layers.
    """

    def read(self, installation: "installation.Installation") -> Any:
        return any(
            "[AQUIFER]" in raw_file.read_text(encoding=RAWFILE_ENCODING)
            for raw_file in self._files(installation.path)
        )

    def write(self, installation: "installation.Installation", value: bool) -> None:
        token_id = "AQUIFER"

        if value is True:
            enable = True
        elif value is False:
            enable = False
        elif isinstance(value, str) and value.lower() == "true":
            enable = True
        elif isinstance(value, str) and value.lower() == "false":
            enable = False
        else:
            raise ValidationError(f"Expected boolean, got {type(value)!s}.")

        if enable is True:
            old, new = f"({token_id})", f"[{token_id}]"
        else:
            old, new = f"[{token_id}]", f"({token_id})"

        replace(self._files(installation.path), old, new, encoding=RAWFILE_ENCODING)

    def _files(self, install_path: Path) -> Iterable[Path]:
        return [
            install_path.joinpath("raw", "objects", filename)
            for filename in [
                "inorganic_stone_layer.txt",
                "inorganic_stone_mineral.txt",
                "inorganic_stone_soil.txt",
            ]
        ]


class PopulationCap(
    DInitFileMixin, IntegerMixin, ClassDocstringHelpMixin, Integer, SimpleTextReplaceSetting,
):
    """
    Prevent the arrival of migrants after a given population cap.
    """

    @property
    def _token_key(self):
        return "POPULATION_CAP"


class StrictPopulationCap(
    DInitFileMixin, IntegerMixin, ClassDocstringHelpMixin, Integer, SimpleTextReplaceSetting,
):
    """
    Prevent the arrival of migrants and stop births after a given cap.
    """

    @property
    def _token_key(self):
        return "STRICT_POPULATION_CAP"


class _ChildrenCap(DInitFileMixin):
    @property
    def _token_key(self):
        return "BABY_CHILD_CAP"

    def _read_raw(self, path: Path) -> Tuple[int, int]:
        match = re.match(
            rf".*\[{self._token_key!s}:(?P<value>[a-zA-Z0-9:]+)\].*",
            path.read_text(encoding=RAWFILE_ENCODING),
            flags=re.M | re.S,
        )

        if match:
            raw_value = match.group("value")
            absolute, percentage = raw_value.split(":", maxsplit=2)
            return int(absolute), int(percentage)
        else:
            raise InvalidRawValue(
                f"Could not find a raw token for {self._token_key} in {path}"
            )


class AbsoluteChildrenCap(_ChildrenCap, ClassDocstringHelpMixin, Integer, SettingDefinition):
    """
    Define a maximum number of dwarven children in the fort.
    """

    def write(self, installation: "installation.Installation", value: PyValue) -> None:
        if int(str(value)) != value:
            raise ValidationError(f"Expected integer, got {type(value)!s}.")

        regex_replace(
            self._file(installation.path),
            rf"(?P<before>.*)"
            rf"\[{self._token_key!s}:\d+:(?P<percentage>\d+)\]"
            rf"(?P<after>.*)",
            rf"\g<before>[{self._token_key!s}:{value!s}:\g<percentage>]\g<after>",
            encoding=RAWFILE_ENCODING,
        )

    def read(self, installation: "installation.Installation") -> int:
        absolute, _ = self._read_raw(self._file(installation.path))
        return absolute


class PercentageChildrenCap(
    _ChildrenCap, ClassDocstringHelpMixin, Integer, SettingDefinition,
):
    """
    Define a maximum percentage of dwarven children vs adults in a fort.
    """

    def write(self, installation: "installation.Installation", value: PyValue) -> None:
        if int(str(value)) != value:
            raise ValidationError(f"Expected integer, got {type(value)!s}.")

        regex_replace(
            self._file(installation.path),
            rf"(?P<before>.*)"
            rf"\[{self._token_key!s}:(?P<absolute>\d+):\d+\]"
            rf"(?P<after>.*)",
            rf"\g<before>[{self._token_key!s}:\g<absolute>:{value!s}]\g<after>",
            encoding=RAWFILE_ENCODING,
        )

    def read(self, installation: "installation.Installation") -> int:
        _, percentage = self._read_raw(self._file(installation.path))
        return percentage


class EnableInvaders(
    DInitFileMixin, BoolMixin, ClassDocstringHelpMixin, Bool, SimpleTextReplaceSetting,
):
    """
    Enable fortress mode sieges.

    Turning this off for your first couple of games may make the game
    easier to start with.
    """

    @property
    def _token_key(self):
        return "INVADERS"


class EnableTemperature(
    DInitFileMixin, BoolMixin, ClassDocstringHelpMixin, Bool, SimpleTextReplaceSetting
):
    """
    Enable temperature calculations.

    If temperature calculations are off, only a few direct temperature
    changes will take place. For example, magma will set tiles and
    creatures to high temperatures, but those tiles will never cool and
    those creatures will not catch fire. Effects that rely on temperature
    calculations, such as water freezing, melting, or evaporating, or
    creatures and items taking temperature-related damage, will not occur.
    """

    @property
    def _token_key(self):
        return "TEMPERATURE"


class EnableWeather(
    DInitFileMixin, BoolMixin, ClassDocstringHelpMixin, Bool, SimpleTextReplaceSetting
):
    """
    Enable weather. If weather is off it will never rain or snow.
    """

    @property
    def _token_key(self):
        return "WEATHER"


class EnableCaveins(
    DInitFileMixin, BoolMixin, ClassDocstringHelpMixin, Bool, SimpleTextReplaceSetting
):
    """
    Enable the possibility for cave-ins to occur.

    A cave-in is when walls, floors, and other terrain plummet downwards
    to lower Z-levels under the influence of gravity. A cave-in will occur
    if constructions or ground tiles are detached from all support
    (bridges and hatches do not provide support).
    """

    @property
    def _token_key(self):
        return "CAVEINS"


class AutosaveFrequency(
    DInitFileMixin,
    StringChoiceMixin,
    ClassDocstringHelpMixin,
    Choice,
    SimpleTextReplaceSetting,
):
    """
    Define when Dwarf Fortress should automatically save your game.

    If this is "NONE", Dwarf Fortress never saves your game for you.
    """

    @property
    def _token_key(self):
        return "AUTOSAVE"

    @property
    def choices(self):
        return ["NONE", "SEASONAL", "YEARLY"]


class EnableAutoBackup(
    DInitFileMixin, BoolMixin, ClassDocstringHelpMixin, Bool, SimpleTextReplaceSetting
):
    """
    Define whether Dwarf Fortress should back up your save for each autodave.
    """

    @property
    def _token_key(self):
        return "AUTOBACKUP"


class EnableAutosavePause(
    DInitFileMixin, BoolMixin, ClassDocstringHelpMixin, Bool, SimpleTextReplaceSetting,
):
    """
    Define whether Dwarf Fortress should pause the game at each autosaves.
    """

    @property
    def _token_key(self):
        return "AUTOSAVE_PAUSE"


class EnablePauseOnLoad(
    DInitFileMixin, BoolMixin, ClassDocstringHelpMixin, Bool, SimpleTextReplaceSetting,
):
    """
    Define whether Dwarf Fortress should pause the game when loaded.
    """

    @property
    def _token_key(self):
        return "PAUSE_ON_LOAD"


class EnableInitialSave(
    DInitFileMixin, BoolMixin, ClassDocstringHelpMixin, Bool, SimpleTextReplaceSetting,
):
    """
    Define whether Dwarf Fortress will save the game immediately after embark.
    """

    @property
    def _token_key(self):
        return "INITIAL_SAVE"


class EnablePetTombsPrevention(
    DInitFileMixin, BoolMixin, ClassDocstringHelpMixin, Bool, SimpleTextReplaceSetting,
):
    """
    Define whether pet should be forbidden to be entombed.

    When building a burial receptacle, the option to allow pets to be
    buried in it will default to NO if this checked.
    """

    @property
    def _token_key(self):
        return "COFFIN_NO_PETS_DEFAULT"


class EnableArtifacts(
    DInitFileMixin, BoolMixin, ClassDocstringHelpMixin, Bool, SimpleTextReplaceSetting
):
    """
    Define whether dwarves should have strange moods and resulting artifacts.
    """

    @property
    def _token_key(self):
        return "ARTIFACTS"


class GrazeCoefficient(
    DInitFileMixin, IntegerMixin, ClassDocstringHelpMixin, Integer, SimpleTextReplaceSetting,
):
    """
    Set this number to scale how often grazing animals need to eat.

    Larger numbers mean less food is necessary.
    Metabolism is always set according to the 3/4ths power of size.
    """

    @property
    def _token_key(self):
        return "GRAZE_COEFFICIENT"


class VisitorCap(
    DInitFileMixin, IntegerMixin, ClassDocstringHelpMixin, Integer, SimpleTextReplaceSetting,
):
    """
    Set the maximum number of visitors to your fort.

    This does not include merchants, diplomats, animals or invaders,
    but only those either dropping by for a temporary visit to a tavern,
    library or temple, or those seeking permanent employment.
    Once you accept a petition from a visitor to stay at your fort,
    they no longer count against the cap, even if they never become a
    full citizen.
    """

    @property
    def _token_key(self):
        return "VISITOR_CAP"


class InvasionSoldierCap(
    DInitFileMixin, IntegerMixin, ClassDocstringHelpMixin, Integer, SimpleTextReplaceSetting,
):
    """
    Set the maximum number of soldiers allowed to invade in serious invasions.

    This doesn't affect smaller raids or mounts, and ambushers and other
    special units will also ignore the cap.  Set INVADERS to NO above if you
    don't want invaders -- a cap of zero will be ignored here.
    """

    @property
    def _token_key(self):
        return "INVASION_SOLDIER_CAP"


class InvasionMonsterCap(
    DInitFileMixin, IntegerMixin, ClassDocstringHelpMixin, Integer, SimpleTextReplaceSetting,
):
    """
    Set the maximum number of monsters allowed to invade in serious invasions.

    This doesn't affect smaller raids or mounts, and ambushers and other
    special units will also ignore the cap.  Set INVADERS to NO above if you
    don't want invaders -- a cap of zero will be ignored here.
    """

    @property
    def _token_key(self):
        return "INVASION_MONSTER_CAP"


class EnableDfhack(ClassDocstringHelpMixin, Bool, SettingDefinition):
    """
    Enable DFHack.
    """

    def read(self, installation: "installation.Installation") -> Any:
        try:
            dfhack = installation.components[ComponentKind.DFHACK]
        except KeyError:
            return False

        if dfhack.release.buildtype.osfamily is OSFamily.WINDOWS:
            sdl_dll_path = installation.path.joinpath("SDL.dll")
            sdl_dll_dfhack_path = installation.path.joinpath("SDLdfhack.dll")

            dll_is_dfhack = same_files(sdl_dll_path, sdl_dll_dfhack_path)
        else:
            dll_is_dfhack = True

        return installation.user_config.enable_dfhack and dll_is_dfhack

    def write(self, installation: "installation.Installation", value: PyValue) -> None:
        try:
            dfhack = installation.components[ComponentKind.DFHACK]
        except KeyError:
            raise ValidationError("Cannot enable DFHack since it is not installed.")

        enable = self._coerce(value)

        config = installation.user_config

        config.enable_dfhack = enable
        config.save(
            installation.path.joinpath(installation.user_config_filename)
        )
        if dfhack.release.buildtype.osfamily is OSFamily.WINDOWS:
            sdl_dll_path = installation.path.joinpath("SDL.dll")
            sdl_dll_real_path = installation.path.joinpath("SDLreal.dll")
            sdl_dll_dfhack_path = installation.path.joinpath("SDLdfhack.dll")

            src_dll = sdl_dll_dfhack_path if enable else sdl_dll_real_path
            shutil.copy(str(src_dll), str(sdl_dll_path))

    def _coerce(self, value: Any) -> bool:
        if value is True:
            return True
        elif value is False:
            return False
        elif isinstance(value, str) and value.lower() == "true":
            return True
        elif isinstance(value, str) and value.lower() == "false":
            return False
        else:
            raise ValidationError(f"Expected boolean, got {type(value)!s}.")
