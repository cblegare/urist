from pathlib import Path
from typing import Any, Dict, Generator, List, Mapping, Optional, Tuple, Type

from pydantic import root_validator, validator

import urist.artifact.setting
from urist import UristError
from urist.artifact.component import ComponentKind
from urist.artifact.distribution import (
    Distribution,
    Distributions,
    DistributionView,
)
from urist.io import yaml
from urist.view import BaseView


class ExistingInstallFolderError(UristError):
    pass


class InstallationNotFound(UristError):
    pass


class ComponentNotFound(UristError):
    pass


class InstallationView(BaseView):
    path: Path
    components: List[DistributionView]

    @validator("components", pre=True)
    def components_validator(cls, v):
        return [DistributionView.from_obj(component) for component in v.values()]


class InstallationsView(BaseView):
    installations: List[InstallationView]

    @classmethod
    def from_obj(cls, obj):
        view = cls(installations=[obj.get(installation) for installation in obj.all()])
        view.__config__.classname = obj.__class__.__name__
        return view


class Installation:
    """
    Represent a Dwarf Fortress installation site.

    An installation lies at a path and is defined by all the components
    that were added to it.  Specific distributions of components
    will need to detect themselves in an installation.
    (see :meth:`urist.artifact.Distributions.list_from_path`).

    A empty folder is a valid installation
    (see :meth:`urist.artifact.Installations.looks_like_df_folder`).

    The first component added to an installation should be the
    Dwarf Fortress game itself.
    """

    user_config_filename = "user_config.yaml"

    def __init__(
        self,
        path: Path,
        distributions: Distributions,
        setting_definitions: Dict[str, "urist.artifact.setting.SettingDefinition"],
    ):
        self._path = path
        self._distributions = distributions
        self._settings = urist.artifact.setting.Settings(self, setting_definitions)

    @property
    def id(self) -> str:
        """
        All installation are identified by their folder name.
        """
        return self._path.name

    @property
    def user_config(self):
        return UserConfig.from_path(self.path.joinpath(self.user_config_filename))

    @property
    def settings(self) -> "urist.artifact.setting.Settings":
        return self._settings

    @property
    def path(self) -> Path:
        return self._path

    def add(self, component: Distribution) -> None:
        """
        Add a distribution of a component to this installation.

        Args:
            component: A distribution of a component which could be
                a graphic pack or else.
        """
        component.install(self._path)

    def remove(self, component: Distribution) -> None:
        """
        Remove a distribution of a component from this installation.

        .. warning::
            Whether or not this function of the specific removed
            distribution should be responsible to restore defaults
            is unclear at this point.

        Args:
            component: A distribution of a component which could be
                a graphic pack or else.
        """
        component.uninstall(self._path)

    def get(self, kind: ComponentKind) -> Distribution:
        try:
            distribution = self.components[kind]
        except IndexError as error:
            raise ComponentNotFound(
                "No '{!s}' component found in install folder '{!s}'.".format(
                    kind, self._path
                )
            ) from error
        else:
            return distribution

    @property
    def components(self) -> Mapping[ComponentKind, Distribution]:
        """
        Provide all component distributions in this installation.
        """
        return {
            distribution.kind: distribution
            for distribution in self._distributions.list_from_path(self._path)
        }

    @property
    def executable(self) -> Path:
        """
        Provide an executable file to play this installation.
        """
        return self.path.joinpath("df")

    def run(self, component: ComponentKind) -> None:
        """
        Launch the game at this installation.
        """
        executable = self.path.joinpath(self.components[component].executable)
        import subprocess

        subprocess.run([str(executable)])


class Installations:
    """
    Manage installations at a given site,

    This class acts as a repository of installations.
    """

    def __init__(
        self,
        path: Path,
        distributions: Distributions,
        settings_definitions: Dict[str, "urist.artifact.setting.SettingDefinition"],
        installation_factory: Optional[Type[Installation]] = None,
    ):
        self._path = path
        self._storage: Dict[str, Installation] = {}
        self._distributions = distributions
        self._settings_definitions = settings_definitions
        self._installation_factory = installation_factory or Installation

    def get_default_installation(self) -> Installation:
        try:
            return list(self._installations.values())[0]
        except IndexError:
            raise InstallationNotFound()

    def add(self, installation_id: str) -> Installation:
        """
        Add a new installation.

        Installations are in the same folder and have all a unique
        name.  That name is their respective folder name.  Hence,
        adding a new installation basically only creates a new
        directory in the installations folder.

        .. note::

            Adding an installation does not make it playable!

            Specific components needs to be added to it (namely the
            game itself).

        Args:
            installation_id: The name of this installation, which
                also define its folder name.

        Returns:
            The create installation object.  Use this to add nice
            components to make your installation playable.

        Raises:
            :class:`ExistingInstallFolderError`: In case an
                installation with this id already exiss.
        """
        installation_path = self._path.joinpath(installation_id)
        try:
            installation_path.mkdir(parents=True, exist_ok=False)
        except FileExistsError as error:
            raise ExistingInstallFolderError(installation_path) from error
        return self._make_installation(installation_path)

    def remove(self, installation_id: str) -> None:
        """
        Remove an installation.

        On removal, all contained components are also removed.

        .. important::
            At this moment, we expect the components to remove all
            of their respective files.  This means that we expect
            that an installation without any component should not
            contain any files either.

            This is a really wild assumption!

        Args:
            installation_id: The name of this installation, which
                also define its folder name.

        Raises:
            :class:`InstallationNotFound`: see
                :meth:`Installations.get`.
        """
        installation = self.get(installation_id)
        for component in reversed(installation.components.values()):
            installation.remove(component)
        self._path.joinpath(installation_id).rmdir()

    def all(self) -> List[str]:
        """
        Provide all found installation ids.
        """
        return [installation_id for installation_id in self._installations.keys()]

    def get(self, installation_id: str) -> Installation:
        """
        Look for an installation and returns it.

        Args:
            installation_id: The name of this installation, which
                also define its folder name.

        Returns:
            Found installation with this id.

        Raises:
            :class:`InstallationNotFound`: In case no installation
                with this id could be found.
        """
        try:
            return self._installations[installation_id]
        except KeyError as error:
            raise InstallationNotFound(installation_id) from error

    def __contains__(self, item: Any) -> bool:
        raise NotImplementedError()

    @property
    def _installations(self) -> Dict[str, Installation]:
        if not self._storage:
            self._storage = {
                identifier: installation
                for identifier, installation in self._gen_installations()
            }
        return self._storage

    def _invalidate_cache(self) -> None:
        self._storage = {}

    def _gen_installations(self,) -> Generator[Tuple[str, Installation], None, None]:
        for path in self._path.glob("*"):
            if self.looks_like_df_folder(path):
                yield path.name, self._make_installation(path)

    def _make_installation(self, path: Path) -> Installation:
        return self._installation_factory(
            path, self._distributions, self._settings_definitions
        )

    @classmethod
    def looks_like_df_folder(cls, path: Path) -> bool:
        """
        Determine if a path looks like a valid Dwarf Fortress Folder.

        A folder looks like a DF folder when it contains an init.txt file
        or is empty.

        Args:
            path: A Dwarf Fortress folder candidate.

        Returns:
            True if the given path looks like a valid Dwarf Fortress
            Folder.
        """
        return path.is_dir() and (
            path.joinpath("data", "init", "init.txt").exists()
            or not any(path.iterdir())
        )


class UserConfig(BaseView):
    enable_dfhack: bool = False

    @classmethod
    def from_path(cls, path: Path):
        try:
            with path.open() as opened:
                values = yaml.load(opened)
        except FileNotFoundError:
            values = {}
        values = values or {}
        obj = cls(**values)
        obj_classname = obj.__class__.__name__
        obj.__config__.classname = obj_classname
        return obj

    def save(self, path: Path):
        self = self.__class__(**self.dict())
        with path.open("w") as opened:
            yaml.dump(self.dict(), opened)
