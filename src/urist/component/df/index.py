import struct
import zlib
from typing import Generator, List

from urist import UristError


class IndexFileError(UristError):
    pass


class DFIndex:
    _RECORD_HEADER_SIZE = 6

    def __init__(self, raw_bytes: bytes):
        """
        DF indices are located at '<df>/data/index' and store static records.

        This file is a sequence of zlib compressed chunks.
        Once deflated, these form a list of record encoded in some
        obscure little endian bytes.

        Args:
            path: Filesystem path to the index file.
        """
        self._raw_bytes = raw_bytes
        self._deflated_bytes = None
        self._records = None

    @classmethod
    def from_index_path(cls, index_file_path):
        return cls(index_file_path.open("rb").read())

    @classmethod
    def from_df_path(cls, df_folder_path):
        return cls(df_folder_path.joinpath("data", "index").open("rb").read())

    @property
    def records(self) -> List[str]:
        if self._records is None:
            try:
                self._deflated_bytes = _SequenceOfZLibCompressedBytes(
                    self._raw_bytes
                ).deflate()
                self._records = [record for record in self._read_records()]
            except (struct.error, zlib.error, ValueError) as error:
                raise IndexFileError("Could not parse index file.") from error

        return self._records

    def _read_records(self) -> Generator[str, None, None]:
        """
        Parse records one at a time.

        Yields:
            Index file records.
        """
        start_position = 4
        while start_position < len(self._deflated_bytes):
            start_of_record_position = start_position + self._RECORD_HEADER_SIZE

            packed_record_header = self._deflated_bytes[
                start_position:start_of_record_position
            ]

            record_size = self._unpack_size_of_record(packed_record_header)

            end_of_record_position = start_of_record_position + record_size

            scrambled_record = self._deflated_bytes[
                start_of_record_position:end_of_record_position
            ]

            record = self._unscramble_record(scrambled_record)

            yield record

            start_position = end_of_record_position

    def _unscramble_record(self, text_as_bytes) -> str:
        """
        Unscrambles one record's bytes.
        """
        return "".join(
            chr(255 - (i % 5) - ch) for i, ch in enumerate(text_as_bytes)
        ).strip()

    def _unpack_size_of_record(self, some_bytes: bytes) -> int:
        """
        Reads the size of a index record bytes.

        The size is encoded as a little endian unsigned long
        and unsigned short that must be equal.

        Args:
            some_bytes: Packed little endian unsigned long and unsigned short.

        Returns:
            The size of the next record.

        Raises:
            ValueError: In case the unsigned long and the unsigned short
                are not equal.
        """
        size, verif = struct.unpack("<LH", some_bytes)
        if size != verif:
            raise ValueError("Record lengths do not match")
        return size


class _SequenceOfZLibCompressedBytes:
    _ZLIB_CHUNK_HEADER_SIZE = 4

    def __init__(self, compressed_bytes: bytes):
        """
        Wrap the zlib compression of the index file.

        Args:
            compressed_bytes: The index file raw data as read in binary mode.
        """
        self._bytes = compressed_bytes

    def deflate(self) -> bytes:
        """
        Deflate the zlib compressed bytes of the index file.

        Returns:
            Scrambled records as bytes.
        """
        return b"".join(some_bytes for some_bytes in self._decompressed_chunks())

    def _decompressed_chunks(self) -> Generator[bytes, None, None]:
        """
        Generate deflated chunks of zlib compressed bytes.

        Yields:
            Deflated chunk.
        """
        start_position = 0
        while start_position < len(self._bytes):
            start_of_chunk_position = start_position + self._ZLIB_CHUNK_HEADER_SIZE

            packed_zlib_chunk_header = self._bytes[
                start_position:start_of_chunk_position
            ]

            chunk_length = self._unpack_size_of_zlibcompressed(packed_zlib_chunk_header)

            end_of_chunk_position = start_of_chunk_position + chunk_length

            deflated_chunk = zlib.decompress(
                self._bytes[start_of_chunk_position:end_of_chunk_position]
            )
            yield deflated_chunk

            start_position = end_of_chunk_position

    def _unpack_size_of_zlibcompressed(self, some_bytes: bytes) -> int:
        """
        Reads the size of a zlib compressed chunks from packed bytes.

        The size is encoded as a little endian unsigned long.

        Args:
            some_bytes: Packed little endian unsigned long

        Returns:
            The size of the following zlib compressed chunk.
        """
        return struct.unpack("<L", some_bytes)[0]
