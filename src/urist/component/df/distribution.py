import logging
import re
from pathlib import Path

import urist.compat
from urist import UristError
from urist.artifact.architecture import Architecture
from urist.artifact.archive import Archive
from urist.artifact.build import Builds
from urist.artifact.buildtype import BuildType
from urist.artifact.component import Component, ComponentKind
from urist.artifact.distribution import Distribution
from urist.artifact.distribution import DistributionFactory as BaseFactory
from urist.artifact.distribution import DistributionNotFound
from urist.artifact.graphics import GraphicalImplementation
from urist.artifact.osfamily import OSFamily
from urist.artifact.release import Release
from urist.artifact.version import Version
from urist.component.df.index import DFIndex, IndexFileError
from urist.db import Database, KnownAsset

log = logging.getLogger(__name__)


class UndetectableArchitecture(UristError):
    pass


class UndetectableGraphicalImplementation(UristError):
    pass


class UndetectableOSFamily(UristError):
    pass


class UndetectableVersion(UristError):
    pass


class Game(Distribution):
    def __init__(
        self,
        component: Component,
        release: Release,
        archive_name: str,
        url: str,
        builds: Builds,
    ):
        super().__init__(
            component, release, archive_name, url,
        )
        self._builds = builds

    def install(self, path: Path) -> None:
        build = self._builds.get(self)
        archive = Archive(build.path)
        archive.extract(path)

    def uninstall(self, path: Path) -> None:
        raise NotImplementedError()

    def is_installed_at(self, path: Path) -> bool:
        raise NotImplementedError()

    @property
    def executable(self) -> Path:
        executable_name_for_osfamily = {
            OSFamily.WINDOWS: "Dwarf Fortress.exe",
            OSFamily.OSX: "df",
            OSFamily.LINUX: "df",
        }
        return Path(executable_name_for_osfamily[self.release.buildtype.osfamily])


class DistributionFactory(BaseFactory):
    KIND = ComponentKind.DF

    def __init__(self, builds: Builds, database: Database):
        self._builds = builds
        self._database = database

    def create_from_orm(self, model: KnownAsset) -> Game:
        kind = model.release.component.kind
        if kind is not self.KIND:
            log.error(
                "Creating Distribution of wrong kind. "
                "Expected {!s} but got {!s}".format(self.KIND, kind)
            )

        component = Component(
            model.release.component.name, model.release.component.kind
        )
        version = Version(model.release.version)
        buildtype = BuildType(model.architecture, model.osfamily, model.graphics)
        release = Release(buildtype, version)
        return Game(component, release, model.archive_name, model.url, self._builds)

    def create_from_path(self, path: Path) -> Game:
        try:
            known_asset: KnownAsset = self._database.query_assets(
                kind=self.KIND,
                graphics=graphics_from_path(path),
                osfamily=osfamily_from_path(path),
                arch=architecture_from_path(path),
                version=df_version_from_path(path),
            ).one()
        except (
            UndetectableGraphicalImplementation,
            UndetectableArchitecture,
            UndetectableVersion,
            UndetectableOSFamily,
        ) as error:
            raise DistributionNotFound() from error
        else:
            return self.create_from_orm(known_asset)


def df_version_from_path(path: Path) -> Version:
    try:
        dfindex = DFIndex(path.joinpath(Path("data", "index")).open("rb").read())
        for record in dfindex.records:
            if re.search(r"\d+~v[\d.a-z]+", record) is not None:
                return Version(record.partition("v")[-1])
            else:
                continue
        else:
            raise IndexFileError("No version record were found.")
    except (FileNotFoundError, IndexFileError) as error:
        raise UndetectableVersion(
            "Could not detect DF version at path {!s}".format(path)
        ) from error


def graphics_from_path(path: Path) -> GraphicalImplementation:
    try:
        init_file = path.joinpath("data/init/init.txt")
        match = re.search(r"\[PRINT_MODE(:.+?)\]", init_file.read_text())
        graphics = (
            GraphicalImplementation.SDL
            if match is not None
            else GraphicalImplementation.LEGACY
        )
    except FileNotFoundError as error:
        raise UndetectableGraphicalImplementation() from error
    else:
        return graphics


def osfamily_from_path(path: Path) -> OSFamily:
    if path.joinpath("fmod.dll").exists():
        osfamily = OSFamily.WINDOWS
    elif path.joinpath("fmod64.dll").exists():
        osfamily = OSFamily.WINDOWS
    elif path.joinpath("._df").exists():
        osfamily = OSFamily.OSX
    elif path.joinpath("README.linux").exists():
        osfamily = OSFamily.LINUX
    else:
        raise UndetectableOSFamily(str(path))

    return osfamily


def architecture_from_path(path: Path) -> Architecture:
    if path.joinpath("fmod64.dll").exists():
        architecture = Architecture.X64
    elif path.joinpath("fmod.dll").exists():
        architecture = Architecture.X86
    elif path.joinpath("libs/libfmodex.dylib").exists():
        architecture = (
            Architecture.X86
            if urist.compat.is_osx_32bits(path.joinpath("libs/libfmodex.dylib"))
            else Architecture.X64
        )
    elif path.joinpath("libs/libgraphics.so").exists():
        architecture = (
            Architecture.X86
            if urist.compat.is_linux_32bits(path.joinpath("libs/libgraphics.so"))
            else Architecture.X64
        )
    else:
        raise UndetectableArchitecture(str(path))
    return architecture
