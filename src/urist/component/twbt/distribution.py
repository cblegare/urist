import logging
import shutil
from pathlib import Path

from urist import UristError
from urist.artifact.archive import Archive
from urist.artifact.build import Builds
from urist.artifact.component import Component
from urist.artifact.distribution import Distribution
from urist.artifact.osfamily import OSFamily
from urist.artifact.release import Release
from urist.io import same_files


class TWBT(Distribution):
    def __init__(
        self,
        component: Component,
        release: Release,
        archive_name: str,
        url: str,
        builds: Builds,
    ):
        super().__init__(
            component, release, archive_name, url,
        )
        self._builds = builds

    def install(self, path: Path) -> None:
        build = self._builds.get(self)
        archive = Archive(build.path)
        archive.extract(path)

        if self.release.buildtype.osfamily is OSFamily.WINDOWS:
            sdl_dll_path = path.joinpath("SDL.dll")
            sdl_dll_real_path = path.joinpath("SDLreal.dll")
            sdl_dll_dfhack_path = path.joinpath("SDLdfhack.dll")
            shutil.copy(str(sdl_dll_path), str(sdl_dll_dfhack_path))
            if not same_files(sdl_dll_path, sdl_dll_dfhack_path):
                raise UristError("Something went wrong installing DFHack.")
            if same_files(sdl_dll_path, sdl_dll_real_path):
                raise UristError("Something went wrong installing DFHack.")
            # Disable by default
            shutil.copy(str(sdl_dll_real_path), str(sdl_dll_path))

    def uninstall(self, path: Path) -> None:
        raise NotImplementedError()

    def is_installed_at(self, path: Path) -> bool:
        raise NotImplementedError()

    @property
    def executable(self) -> Path:
        if self.release.buildtype.osfamily is OSFamily.WINDOWS:
            return Path("df")
        else:
            return Path("dfhack")