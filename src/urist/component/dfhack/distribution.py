import logging
import re
import shutil
from pathlib import Path

from urist import UristError
from urist.artifact.archive import Archive
from urist.artifact.build import Builds
from urist.artifact.buildtype import BuildType
from urist.artifact.component import Component, ComponentKind
from urist.artifact.distribution import Distribution
from urist.artifact.distribution import DistributionFactory as BaseFactory
from urist.artifact.distribution import DistributionNotFound
from urist.artifact.osfamily import OSFamily
from urist.artifact.release import Release
from urist.artifact.version import Version
from urist.component.df.distribution import (
    UndetectableArchitecture,
    UndetectableGraphicalImplementation,
    UndetectableOSFamily,
    architecture_from_path,
    graphics_from_path,
    osfamily_from_path,
)
from urist.db import Database, KnownAsset
from urist.io import same_files

log = logging.getLogger(__name__)


class DFHack(Distribution):
    def __init__(
        self,
        component: Component,
        release: Release,
        archive_name: str,
        url: str,
        builds: Builds,
    ):
        super().__init__(
            component, release, archive_name, url,
        )
        self._builds = builds

    def install(self, path: Path) -> None:
        build = self._builds.get(self)
        archive = Archive(build.path)
        archive.extract(path)

        if self.release.buildtype.osfamily is OSFamily.WINDOWS:
            sdl_dll_path = path.joinpath("SDL.dll")
            sdl_dll_real_path = path.joinpath("SDLreal.dll")
            sdl_dll_dfhack_path = path.joinpath("SDLdfhack.dll")
            shutil.copy(str(sdl_dll_path), str(sdl_dll_dfhack_path))
            if not same_files(sdl_dll_path, sdl_dll_dfhack_path):
                raise UristError("Something went wrong installing DFHack.")
            if same_files(sdl_dll_path, sdl_dll_real_path):
                raise UristError("Something went wrong installing DFHack.")
            # Disable by default
            shutil.copy(str(sdl_dll_real_path), str(sdl_dll_path))

    def uninstall(self, path: Path) -> None:
        raise NotImplementedError()

    def is_installed_at(self, path: Path) -> bool:
        raise NotImplementedError()

    @property
    def executable(self) -> Path:
        if self.release.buildtype.osfamily is OSFamily.WINDOWS:
            return Path("df")
        else:
            return Path("dfhack")


class DistributionFactory(BaseFactory):
    KIND = ComponentKind.DFHACK

    def __init__(self, builds: Builds, database: Database):
        self._builds = builds
        self._database = database

    def create_from_orm(self, model: KnownAsset) -> Distribution:
        kind = model.release.component.kind
        if kind is not self.KIND:
            log.error(
                "Creating Distribution of wrong kind. "
                "Expected {!s} but got {!s}".format(self.KIND, kind)
            )

        component = Component(
            model.release.component.name, model.release.component.kind
        )
        version = Version(model.release.version)
        buildtype = BuildType(model.architecture, model.osfamily, model.graphics)
        release = Release(buildtype, version)
        return DFHack(component, release, model.archive_name, model.url, self._builds)

    def create_from_path(self, path: Path) -> Distribution:
        try:
            with path.joinpath("hack/news.rst").open() as opened:
                first_line = opened.readline()
                m = re.match(r"DFHack (?P<version>\d\.\d+.\d+[\-a-z0-9]+)", first_line)
                dfhack_version = m.group("version")

            known_asset: KnownAsset = self._database.query_assets(
                kind=self.KIND,
                graphics=graphics_from_path(path),
                osfamily=osfamily_from_path(path),
                arch=architecture_from_path(path),
                version=Version(dfhack_version),
            ).all()[0]
        except (
            UndetectableGraphicalImplementation,
            UndetectableArchitecture,
            UndetectableOSFamily,
            FileNotFoundError,
            IndexError,
        ) as error:
            raise DistributionNotFound() from error
        else:
            return self.create_from_orm(known_asset)
