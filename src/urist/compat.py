import struct
import sys
from pathlib import Path
from typing import Any

from elftools.elf.elffile import ELFFile
from macholib.mach_o import mach_header_64
from macholib.MachO import MachO

windows = "win32" in str(sys.platform).lower()
linux = "linux" in str(sys.platform).lower()
osx = "darwin" in str(sys.platform).lower()

_sizeof_void_pointer = struct.calcsize("P")
bit32 = _sizeof_void_pointer * 8 == 32
bit64 = _sizeof_void_pointer * 8 == 64


class HashableMixin:
    def __hash__(self) -> int:
        return hash(tuple(sorted(self.__dict__.items())))

    def __eq__(self, other: Any) -> bool:
        return hash(self) == hash(other)


def is_osx_32bits(path: Path):
    macho = MachO(str(path))
    try:
        for header in macho.headers:
            if isinstance(header.mach_header(), mach_header_64):
                return True
        else:
            return False
    except:
        return False


def is_linux_32bits(path: Path):
    try:
        with path.open("rb") as opened:
            return ELFFile(opened).elfclass == 32
    except:
        return False
