from pathlib import Path
from typing import Optional

from urist.io import snake_case
from urist.view import BaseView


class Env:
    def __init__(
        self,
        data_dir: Path = None,
        cache_dir: Optional[Path] = None,
        database_url: Optional[str] = None,
        do_update_db: bool = True,
        dry_run: bool = False,
    ):
        self.data_dir = data_dir
        self.cache_dir = cache_dir or Path(data_dir, "cache")
        self.database_url = database_url or f"sqlite:///{self.data_dir!s}/db.db"
        self.do_update_db = do_update_db
        self.dry_run = dry_run


class EnvView(BaseView):
    data_dir: Path
    cache_dir: Path
    database_url: str
    do_update_db: bool
