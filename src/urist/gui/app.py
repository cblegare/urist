from pathlib import Path
from typing import List, cast

from qtpy import QtWidgets

from urist.app import Urist
from urist.gui.bridge import InstallationBridge, SettingBridge
from urist.gui.mainwindow import UristUI
from urist.log import get_logger
from urist.servicelocator import ContainerBuilder

log = get_logger(__name__)


class Application(QtWidgets.QApplication):
    """
    Qt Application, required before any Qt GUI is created.
    """

    def __init__(self, args: List[str]):
        super().__init__(args)


class GUI:
    """
    On convenient container for all GUI things.
    """

    def __init__(
        self,
        urist: Urist,
        qt_app: Application,
        installation_bridge: InstallationBridge,
        setting_bridge: SettingBridge,
        mainwindow: UristUI,
    ):
        self.urist = urist
        self.qt_app = qt_app
        self.installation_bridge = installation_bridge
        self.setting_bridge = setting_bridge
        self.mainwindow = mainwindow

    def start(self) -> None:
        """
        Start the GUI main loop.
        """
        self.installation_bridge.current_installation_changed.emit()
        self.mainwindow.show()
        self.qt_app.exec_()


def make_gui(urist: Urist) -> GUI:
    container_builder = ContainerBuilder()
    container_builder.register(GUI)
    container_builder.register(lambda: urist, "urist")
    container_builder.register(lambda: Application([]), "qt_app")
    container_builder.register(installation_bridge)
    container_builder.register(setting_bridge)
    container_builder.register(mainwindow)

    container = container_builder.build()

    return cast(GUI, container.get(GUI.__name__))


def installation_bridge(urist: Urist, qt_app: Application) -> InstallationBridge:
    return InstallationBridge(urist)


def setting_bridge(
    urist: Urist, installation_bridge: InstallationBridge, qt_app: Application
) -> SettingBridge:
    return SettingBridge(installation_bridge, urist)


def mainwindow(
    urist: Urist,
    installation_bridge: InstallationBridge,
    setting_bridge: SettingBridge,
    qt_app: Application,
) -> UristUI:
    return UristUI(urist, installation_bridge, setting_bridge)
