from pathlib import Path

from qtpy import QtWidgets, uic

from urist.log import get_logger

log = get_logger(__name__)


def load_ui(name: str, **kwargs) -> QtWidgets.QWidget:
    ui_path = Path(__file__).parent.joinpath(f"{name}.ui")
    log.debug(f"Loading ui '{name}' at '{ui_path!s}'.")
    widget = uic.loadUi(str(ui_path), **kwargs)
    return widget
