from typing import Any, Callable

from qtpy import QtCore, QtGui, QtWidgets, uic

import urist.gui.bridge
from urist.artifact.architecture import Architecture
from urist.artifact.buildtype import BuildType
from urist.artifact.graphics import GraphicalImplementation
from urist.artifact.osfamily import OSFamily
from urist.cmd import CreateInstall, NoOp
from urist.gui.ui import load_ui

UiLoader = Callable[[str, QtWidgets.QWidget], QtWidgets.QWidget]


class AddInstall(QtWidgets.QDialog):
    def __init__(self, bridge: "urist.gui.bridge.InstallationBridge", parent=None):
        super().__init__(parent=parent)
        self._bridge = bridge

    @classmethod
    def get_cmd(
        cls,
        bridge: "urist.gui.bridge.InstallationBridge",
        parent: QtWidgets.QWidget = None,
    ):
        dialog = cls(bridge, parent=parent)
        dialog = load_ui("install", baseinstance=dialog)

        installation_id_edit = dialog.findChild(QtWidgets.QLineEdit, "installation_id")
        dirname_regexp = QtCore.QRegExp(r"^[\w]+$")
        validator = QtGui.QRegExpValidator(dirname_regexp)
        installation_id_edit.setValidator(validator)

        version_cbb = dialog.findChild(QtWidgets.QComboBox, "version")
        version_cbb.addItems(sorted(bridge.available_df_versions, reverse=True))

        osfamily_cbb = dialog.findChild(QtWidgets.QComboBox, "osfamily")
        osfamily_cbb.addItems([choice.value for choice in OSFamily.get_choices()])
        osfamily_cbb.setCurrentText(str(OSFamily.from_platform()))

        architecture_cbb = dialog.findChild(QtWidgets.QComboBox, "architecture")
        architecture_cbb.addItems(
            [choice.value for choice in Architecture.get_choices()]
        )
        architecture_cbb.setCurrentText(str(Architecture.from_platform()))

        graphics_cbb = dialog.findChild(QtWidgets.QComboBox, "graphics")
        graphics_cbb.addItems(
            [choice.value for choice in GraphicalImplementation.get_choices()]
        )
        graphics_cbb.setCurrentText(str(GraphicalImplementation.from_platform()))

        dialog.exec_()
        if dialog.result() == QtWidgets.QDialog.Accepted:
            return CreateInstall(
                version=version_cbb.currentText(),
                build_type=BuildType(
                    Architecture(architecture_cbb.currentText()),
                    OSFamily(osfamily_cbb.currentText()),
                    GraphicalImplementation(graphics_cbb.currentText()),
                ),
                installation_id=installation_id_edit.text(),
            )
        else:
            return NoOp()
