import logging
from pathlib import Path

from qtpy import QtWidgets

from urist.app import Urist
from urist.cmd import Play
from urist.gui.bridge import InstallationBridge, SettingBridge
from urist.gui.ui import load_ui

log = logging.getLogger(__name__)


class UristUI(QtWidgets.QMainWindow):
    def __init__(
        self,
        urist: Urist,
        installation_bridge: InstallationBridge,
        setting_bridge: SettingBridge,
        parent=None,
    ):
        super().__init__(parent=parent)
        self._urist = urist
        self._installation = installation_bridge
        self._settings = setting_bridge

        self._ui = load_ui("urist", baseinstance=self)
        self._installation.setup_ui(self._ui)
        self._settings.setup_ui(self._ui)

        self._ui.play_btn.clicked.connect(self.play)

    def play(self) -> None:
        installation_id = self._installation.current_installation.id
        cmd = Play(installation_id)
        self._urist.do(cmd)
