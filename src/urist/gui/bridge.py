import logging
from typing import Any, List, Optional, Set, Union

from qtpy import QtCore, QtWidgets

from urist import UristError
from urist.app import Urist
from urist.artifact.component import ComponentKind
from urist.artifact.installation import Installation, InstallationNotFound
from urist.cmd import SetSetting
from urist.gui.addinstall import AddInstall
from urist.io import rst_to_html
from urist.artifact.setting import Bool, Integer, Choice

log = logging.getLogger(__name__)


class InstallationBridge(QtCore.QObject):
    current_installation_changed = QtCore.Signal()
    installation_list_changed = QtCore.Signal()
    installations_changed = QtCore.Signal()

    def __init__(self, urist: Urist, parent: Optional[QtCore.QObject] = None):
        """
        Represents the Urist backend when used in a Qt context.

        This class will emit signals when the file system changed, for
        instance.

        This class also provide slots for interacting with configurations,
        installations and other compnents.  Slots should map directly to
        commands as in :mod:`urist.cmd`.

        Args:
            urist: The Urist API.
            parent: A QObject parent.
        """
        super().__init__(parent)
        log.debug("Initalizing Urist Qt bridge.")
        self._urist = urist
        try:
            self._selected_installation: Optional[
                Installation
            ] = self._urist.installations.get_default_installation()
            log.debug(
                "Set default installation to {!s}.".format(
                    self._selected_installation.id
                )
            )
        except InstallationNotFound:
            log.error("No installation found.")
            self._selected_installation = None

    def setup_ui(self, ui: QtWidgets.QWidget):
        log.debug("Setup UI for installations.")
        ui.add_installation_btn.clicked.connect(self.run_installation_wizard)
        ui.installation_choice.activated[str].connect(self.select_installation)
        self.installation_list_changed.connect(
            self.make_installation_choice_refresher(ui.installation_choice)
        )
        self.current_installation_changed.connect(
            self.make_installation_choice_refresher(ui.installation_choice)
        )

    def run_installation_wizard(self):
        log.debug("Opening installation dialog")
        cmd = AddInstall.get_cmd(self)
        self.execute(cmd)
        self.installation_list_changed.emit()

    def select_installation(self, new_installation_id: str) -> None:
        """
        Request change of installation choice to the backend.

        Args:
            new_installation_id: Selected installation.
        """
        log.debug("Installation choice changed. Forwarding to backend...")
        self._selected_installation = self._urist.installations.get(new_installation_id)
        log.debug(f"Broadcast current installation changed tp '{new_installation_id}'.")
        self.current_installation_changed.emit()

    def make_installation_choice_refresher(self, widget: QtWidgets.QComboBox):
        def slot():
            log.debug("Available installations changed.  Refreshing UI...")
            installations = self._urist.installations.all()
            current = self.current_installation
            widget.clear()
            widget.addItems(installations)

            if current is not None:
                widget.setCurrentText(current.id)

        return slot

    @property
    def current_installation(self) -> Union[Installation, None]:
        return self._selected_installation

    @QtCore.Slot()
    def play(self) -> None:
        log.debug("Initiating PLAY")

    @property
    def available_df_versions(self) -> Set[str]:
        return set(
            str(distribution.release.version)
            for distribution in self._urist.distributions.find_all(
                kind=ComponentKind.DF
            )
        )

    def execute(self, cmd):
        self._urist.do(cmd)


class SettingBridge(QtCore.QObject):
    setting_changed = QtCore.Signal()

    def __init__(
        self, bridge: InstallationBridge, urist: Urist, parent: QtCore.QObject = None
    ):
        super().__init__(parent=parent)
        self._bridge = bridge
        self._urist = urist
        self._setting_models = []

    def setup_ui(self, ui: QtWidgets.QWidget):
        log.debug("Setup UI for settings.")
        for setting_id, setting_definition in self._urist.settings_definitions.items():
            log.debug(f"Setup settings '{setting_id}'.")
            classes = {
                Integer: QtWidgets.QSpinBox,
                Bool: QtWidgets.QCheckBox,
                Choice: QtWidgets.QComboBox
            }
            control = None
            for cls in classes:
                if isinstance(setting_definition, cls):
                    control = ui.findChild(classes[cls], setting_id)
                    break
            else:
                raise UristError(f"Could not find controle for setting '{setting_id}'.")

            if isinstance(control, QtWidgets.QSpinBox):
                control.setMaximum(9999)

            self._connect_widget_value_changed(control, setting_id)
            self._connect_setting_changed(control, setting_id)

            related_label = ui.findChild(QtWidgets.QLabel, f"{setting_id}_lbl")
            related_groupbox = ui.findChild(QtWidgets.QGroupBox, f"{setting_id}_grp")

            for widget in [control, related_label, related_groupbox]:
                if widget is not None:
                    self._connect_installation_changed(widget, setting_id)

    def _connect_widget_value_changed(self, widget: QtWidgets.QWidget, setting_id: str):
        if isinstance(widget, QtWidgets.QSpinBox):
            widget.valueChanged.connect(self._set_setting_from_spinbox(setting_id))
        elif isinstance(widget, QtWidgets.QComboBox):
            widget.activated[str].connect(self._set_setting_from_combobox(setting_id))
        elif isinstance(widget, QtWidgets.QCheckBox):
            widget.stateChanged.connect(self._set_setting_from_checkbox(setting_id))

    def _connect_installation_changed(self, widget: QtWidgets.QWidget, setting_id: str):
        connect_this = self._bridge.current_installation_changed
        if isinstance(widget, QtWidgets.QSpinBox):
            connect_this.connect(self._refresh_spinbox(widget, setting_id))
        if isinstance(widget, QtWidgets.QComboBox):
            connect_this.connect(self._refresh_combobox(widget, setting_id))
        if isinstance(widget, QtWidgets.QCheckBox):
            connect_this.connect(self._refresh_checkbox(widget, setting_id))
        if isinstance(widget, (QtWidgets.QLabel, QtWidgets.QGroupBox)):
            connect_this.connect(self._refresh_decoration(widget, setting_id))

    def _connect_setting_changed(self, widget: QtWidgets.QWidget, setting_id: str):
        connect_this = self.setting_changed
        if isinstance(widget, QtWidgets.QSpinBox):
            connect_this.connect(self._refresh_spinbox(widget, setting_id))
        if isinstance(widget, QtWidgets.QComboBox):
            connect_this.connect(self._refresh_combobox(widget, setting_id))
        if isinstance(widget, QtWidgets.QCheckBox):
            connect_this.connect(self._refresh_checkbox(widget, setting_id))

    def _refresh_spinbox(self, widget: QtWidgets.QSpinBox, setting_id: str):
        def slot():
            # We could catch exceptions here:
            #   Say a setting does not exist in given installation
            #   - Catch "UnsupportedSetting"
            #   - Disable the widget
            value = self.get_setting_value(setting_id)
            widget.setToolTip(self.get_setting_help(setting_id))
            widget.setValue(int(value))

        return slot

    def _refresh_combobox(self, widget: QtWidgets.QComboBox, setting_id: str):
        def slot():
            value = self.get_setting_value(setting_id)
            widget.clear()
            widget.addItems(self.get_choices(setting_id))
            widget.setToolTip(self.get_setting_help(setting_id))
            widget.setCurrentText(str(value))

        return slot

    def _refresh_checkbox(self, widget: QtWidgets.QCheckBox, setting_id: str):
        def slot():
            value = self.get_setting_value(setting_id)
            widget.setToolTip(self.get_setting_help(setting_id))
            widget.setCheckState(
                QtCore.Qt.Checked if bool(value) else QtCore.Qt.Unchecked
            )

        return slot

    def _refresh_decoration(self, widget: QtWidgets.QLabel, setting_id: str):
        def slot():
            widget.setToolTip(self.get_setting_help(setting_id))

        return slot

    def _set_setting_from_spinbox(self, setting_id: str):
        def slot(value: int):
            self.set_setting_value(setting_id, value)

        return slot

    def _set_setting_from_combobox(self, setting_id: str):
        def slot(value: str):
            self.set_setting_value(setting_id, value)

        return slot

    def _set_setting_from_checkbox(self, setting_id: str):
        def slot(state: int):
            self.set_setting_value(setting_id, state == QtCore.Qt.Checked)

        return slot

    def set_setting_value(self, setting_id: str, value: Union[str, int, bool]):
        if self.get_setting_value(setting_id) == value:
            log.debug(f"Value for '{setting_id}' did not change, skipping write,")
        else:
            cmd = SetSetting(self._bridge.current_installation.id, setting_id, value)
            self._urist.do(cmd)
            self.setting_changed.emit()

    def get_choices(self, setting_id: str) -> List[str]:
        return self._urist.settings_definitions[setting_id].choices

    def get_setting_value(self, setting_id: str) -> Union[str, int, bool]:
        return self._bridge.current_installation.settings.get_value(setting_id)

    def get_setting_help(self, setting_id: str) -> str:
        return rst_to_html(
            self._bridge.current_installation.settings.get_help(setting_id)
        )
