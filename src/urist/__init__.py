from pkg_resources import Distribution, DistributionNotFound, get_distribution

_PACKAGE = "urist"


try:
    __dist__: Distribution = get_distribution(_PACKAGE)
except DistributionNotFound:
    __dist__ = Distribution(project_name=_PACKAGE, version="(local)")


class UristError(RuntimeError):
    pass
