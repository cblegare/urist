from typing import Dict, cast

import urist.artifact.setting
import urist.cfg
import urist.component.df.distribution
import urist.component.dfhack.distribution
import urist.db
from urist import cmd
from urist.artifact.build import Builds
from urist.artifact.distribution import Distributions
from urist.artifact.installation import Installations
from urist.cfg import Env
from urist.io import snake_case
from urist.log import get_logger
from urist.servicelocator import ContainerBuilder

log = get_logger(__name__)


class Urist:
    def __init__(
        self,
        config: urist.cfg.Env,
        database: urist.db.Database,
        builds: Builds,
        distributions: Distributions,
        installations: Installations,
        settings_definitions: Dict[str, urist.artifact.setting.SettingDefinition],
    ):
        self.config = config
        self.database = database
        self.builds = builds
        self.distributions = distributions
        self.installations = installations
        self.settings_definitions = settings_definitions

        self.config.data_dir.mkdir(parents=True, exist_ok=True)
        self.config.cache_dir.mkdir(parents=True, exist_ok=True)

        if self.config.do_update_db:
            self.database.migrate()

    def do(self, command: "cmd.Command"):
        log.info(f"Executing command {repr(command)}")
        log.debug(command.view.yaml())
        if self.config.dry_run:
            log.warning("DRYRUN: Not executing command")
        else:
            command.execute(self)

    def get_settings(self, installation_id):
        installation = self.installations.get(installation_id)
        return urist.artifact.setting.Settings(installation, self.settings_definitions)


def make_urist(_config: Env) -> Urist:
    container_builder = ContainerBuilder()
    container_builder.register(lambda: _config, "config")

    container_builder.register(builds)
    container_builder.register(database)
    container_builder.register(distributions)
    container_builder.register(installations)
    container_builder.register(settings_definitions)

    container_builder.register(Urist)

    container = container_builder.build()

    return cast(Urist, container.get(Urist.__name__))


def builds(config: Env) -> Builds:
    storage = config.cache_dir.joinpath("builds")
    return Builds(storage)


def database(config: Env) -> urist.db.Database:
    db = urist.db.Database(config.database_url)
    return db


def distributions(database: urist.db.Database, builds: Builds) -> Distributions:
    import urist.component.df

    return Distributions(
        database,
        {
            urist.component.df.distribution.DistributionFactory.KIND: urist.component.df.distribution.DistributionFactory(
                builds, database
            ),
            urist.component.dfhack.distribution.DistributionFactory.KIND: urist.component.dfhack.distribution.DistributionFactory(
                builds, database
            ),
        },
    )


def settings_definitions() -> Dict[str, urist.artifact.setting.SettingDefinition]:
    return {
        snake_case(setting_definition.__class__.__name__): setting_definition
        for setting_definition in [
            urist.artifact.setting.EnableAquifer(),
            urist.artifact.setting.PopulationCap(),
            urist.artifact.setting.StrictPopulationCap(),
            urist.artifact.setting.AbsoluteChildrenCap(),
            urist.artifact.setting.PercentageChildrenCap(),
            urist.artifact.setting.EnableInvaders(),
            urist.artifact.setting.EnableTemperature(),
            urist.artifact.setting.EnableWeather(),
            urist.artifact.setting.EnableCaveins(),
            urist.artifact.setting.AutosaveFrequency(),
            urist.artifact.setting.EnableAutoBackup(),
            urist.artifact.setting.EnableAutosavePause(),
            urist.artifact.setting.EnablePauseOnLoad(),
            urist.artifact.setting.EnableInitialSave(),
            urist.artifact.setting.EnablePetTombsPrevention(),
            urist.artifact.setting.EnableArtifacts(),
            urist.artifact.setting.GrazeCoefficient(),
            urist.artifact.setting.VisitorCap(),
            urist.artifact.setting.InvasionSoldierCap(),
            urist.artifact.setting.InvasionMonsterCap(),
            urist.artifact.setting.EnableDfhack(),
        ]
    }


def installations(
    config: urist.cfg.Env,
    distributions: Distributions,
    settings_definitions: Dict[str, urist.artifact.setting.SettingDefinition],
) -> Installations:
    installation_dir = config.data_dir.joinpath("installations")
    installation_dir.mkdir(exist_ok=True, parents=True)
    return Installations(installation_dir, distributions, settings_definitions)
