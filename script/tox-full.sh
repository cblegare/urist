#!/bin/sh
mypy src
tox -e \
style,flake8-codeclimate,flake8-junit,\
src_complexity,test_complexity,\
build_pydists,build_standalone,\
clean,py{36,37},report,\
security,security_reports,\
doc
